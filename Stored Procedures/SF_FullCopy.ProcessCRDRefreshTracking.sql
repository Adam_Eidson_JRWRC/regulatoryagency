SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 6/30/2020
-- Description:	Process CRD to insert or update the LastQuery column so we can later batch requests to FINRA/SEC
-- =============================================
CREATE PROCEDURE [SF_FullCopy].[ProcessCRDRefreshTracking]
	-- Add the parameters for the stored procedure here
	@CRD bigint,
	@ObjectType varchar(12)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Test if CRD exists
	if not exists(select CRD 
			from RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking 
			where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
			--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 
			)
		BEGIN
		
			--if not, insert
			insert into RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking 
			(
			CRD, 
			ObjectType
			)

			select @CRD,
			@ObjectType

		END

	ELSE 
		BEGIN
		--else, update
		update crt
		set LastQuery = GETDATE()
		from RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking crt
		where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
		--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 

		select *
		from RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking crt
		where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
		--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 

		END

	

	
END
GO
GRANT EXECUTE ON  [SF_FullCopy].[ProcessCRDRefreshTracking] TO [ETL_User]
GO
