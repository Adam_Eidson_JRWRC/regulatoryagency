SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 8/26/2020
-- Description:	List of prepped Contact data to insert into SF
-- =============================================
CREATE PROCEDURE [SF_FullCopy].[PrioritizedFirmsContactsToInsert]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select *
	into #tempvContactsWithCRD
	from SalesForce_reporting.[SF_FULLCOPY].vContactsWithCRD

select *
--drop table #tempSFFirms
into #tempSFFirms
From SalesForce_Reporting.[SF_FULLCOPY].[vPrioritizedFirmsToKeepSyncedWithFINRASEC]

SELECT distinct [CRDNumber]      
into #tempKeepCRD
  FROM [RegulatoryAgency].[dbo].[vIndividualLicense]
  where REPLACE(LicenseName, 'Series ','') in ('7','22','7TO','22TO','65')

;with cte_KeepCRD as (
SELECT distinct [CRDNumber]      
  FROM [RegulatoryAgency].[dbo].[vIndividualLicense]
  where REPLACE(LicenseName, 'Series ','') in ('7','22','7TO','22TO','65')
  ), cte_ValidLicense as (
  select vilf.*
  from [RegulatoryAgency].[dbo].[vIndividualLicense_Flattened] vilf
  left join cte_KeepCRD ckc on ckc.CRDNumber = vilf.CRDNumber
  where ckc.CRDNumber is not null
  )

  select *
  into #tempValidLicense
  from cte_ValidLicense

select  
firmCRDNumber,
count(*) as CountNewIndividuals
--individualCRDNumber as CRD__c,
--Date_Became_Rep__c,
--prep.FirstName,
--MiddleName,
--prep.LastName,
--Suffix,
--FINRALink as FINRA_Detailed_Report_URL__c,
----SECLink,
----crt.id as RecordTypeId ,
--case when cvl.Licenses is not null
--then 'active'
--else 'inactive'
--end as Status__c,
--vilf.Licenses as Licenses_Held__c,
--'FINRA/SEC ETL' as LeadSource
--address
into #tempValidFirms
from (
select  distinct
vif.individualCRDNumber,
case when i.dateBeganInIndustry is not null
then i.dateBeganInIndustry
end as Date_Became_Rep__c,
i.FirstName,
i.MiddleName,
i.LastName,
i.Suffix,
i.FINRAStatus, i.SECStatus,
--RegulatoryAgency.dbo.ProperCase(i.FirstName) as FirstName,
--RegulatoryAgency.dbo.ProperCase(i.MiddleName) as MiddleName,
--RegulatoryAgency.dbo.ProperCase(i.LastName) as LastName,
--RegulatoryAgency.dbo.ProperCase(i.Suffix) as Suffix,
--RegulatoryAgency.dbo.[ContactRecordType](i.FINRAStatus, i.SECStatus) as RecordType,
--RegulatoryAgency.dbo.[ContactFINRAStatus](i.FINRAStatus, i.SECStatus) as FINRA_Status__c,
case when i.FINRALink is not null
then i.FINRALink
end as FINRALink,
case when i.SECLink is not null
then i.SECLink
end as SECLink,
vif.firmCRDNumber
from RegulatoryAgency.dbo.vIndividualFirm vif
join #tempKeepCRD ckc on ckc.CRDNumber = vif.individualCRDNumber
join RegulatoryAgency.dbo.Individual i on i.CRDNumber = vif.individualCRDNumber
join #tempSFFirms SFPFirm on SFPFIRM.Firm_CRD__c = cast(vif.firmCRDNumber as varchar(15)) 
where vif.EndDate is null
and RecordType in ('Broker-Dealer', 'Dually Registered')
) prep
--left join SalesForce_Reporting.[SF_FULLCOPY].vContactRecordType crt on crt.name = prep.RecordType
left join #tempValidLicense cvl on cvl.CRDNumber = prep.individualCRDNumber
left join RegulatoryAgency.dbo.vIndividualLicense_Flattened vilf on vilf.CRDNumber = prep.individualCRDNumber
left join #tempvContactsWithCRD SF_Contact on SF_Contact.CRD__c = prep.individualCRDNumber

where SF_Contact.id is null

group by firmCRDNumber
having count(*) <=100



select  distinct
individualCRDNumber as CRD__c,
Date_Became_Rep__c,
prep.FirstName,
MiddleName,
prep.LastName,
Suffix,
FINRALink as FINRA_Detailed_Report_URL__c,
--SECLink,
crt.id as RecordTypeId ,
case when cvl.Licenses is not null
then 'active'
else 'inactive'
end as Status__c,
vilf.Licenses as Licenses_Held__c,
prep.FINRA_Status__c, 
'FINRA/SEC ETL' as LeadSource
--address
from (
select  distinct
vif.individualCRDNumber,
case when i.dateBeganInIndustry is not null
then i.dateBeganInIndustry
end as Date_Became_Rep__c,
RegulatoryAgency.dbo.ProperCase(i.FirstName) as FirstName,
RegulatoryAgency.dbo.ProperCase(i.MiddleName) as MiddleName,
RegulatoryAgency.dbo.ProperCase(i.LastName) as LastName,
RegulatoryAgency.dbo.ProperCase(i.Suffix) as Suffix,
RegulatoryAgency.dbo.[ContactRecordType](i.FINRAStatus, i.SECStatus) as RecordType,
RegulatoryAgency.dbo.[ContactFINRAStatus](i.FINRAStatus, i.SECStatus) as FINRA_Status__c,
case when i.FINRALink is not null
then i.FINRALink
end as FINRALink,
case when i.SECLink is not null
then i.SECLink
end as SECLink,
vif.firmCRDNumber
from 
#tempValidFirms tvf
join RegulatoryAgency.dbo.vIndividualFirm vif on tvf.firmCRDNumber = vif.firmCRDNumber
join #tempKeepCRD ckc on ckc.CRDNumber = vif.individualCRDNumber
join RegulatoryAgency.dbo.Individual i on i.CRDNumber = vif.individualCRDNumber
join #tempSFFirms SFPFirm on SFPFIRM.Firm_CRD__c = cast(vif.firmCRDNumber as varchar(15)) 
where vif.EndDate is null) prep
left join SalesForce_Reporting.[SF_FULLCOPY].vContactRecordType crt on crt.name = prep.RecordType
left join #tempValidLicense cvl on cvl.CRDNumber = prep.individualCRDNumber
left join RegulatoryAgency.dbo.vIndividualLicense_Flattened vilf on vilf.CRDNumber = prep.individualCRDNumber
left join #tempvContactsWithCRD SF_Contact on SF_Contact.CRD__c = prep.individualCRDNumber

where SF_Contact.id is null

union




select distinct 
individualCRDNumber as CRD__c,
Date_Became_Rep__c,
prep.FirstName,
MiddleName,
prep.LastName,
Suffix,
FINRALink as FINRA_Detailed_Report_URL__c,
--SECLink,
crt.id as RecordTypeId ,
case when cvl.Licenses is not null
then 'active'
else 'inactive'
end as Status__c,
vilf.Licenses as Licenses_Held__c,
prep.FINRA_Status__c, 
'FINRA/SEC ETL' as LeadSource
--address
from (
select  distinct
vif.individualCRDNumber,
case when i.dateBeganInIndustry is not null
then i.dateBeganInIndustry
end as Date_Became_Rep__c,
RegulatoryAgency.dbo.ProperCase(i.FirstName) as FirstName,
RegulatoryAgency.dbo.ProperCase(i.MiddleName) as MiddleName,
RegulatoryAgency.dbo.ProperCase(i.LastName) as LastName,
RegulatoryAgency.dbo.ProperCase(i.Suffix) as Suffix,
RegulatoryAgency.dbo.[ContactRecordType](i.FINRAStatus, i.SECStatus) as RecordType,
RegulatoryAgency.dbo.[ContactFINRAStatus](i.FINRAStatus, i.SECStatus) as FINRA_Status__c,
case when i.FINRALink is not null
then i.FINRALink
end as FINRALink,
case when i.SECLink is not null
then i.SECLink
end as SECLink,
vif.firmCRDNumber
from RegulatoryAgency.dbo.vIndividualFirm vif 
join #tempKeepCRD ckc on ckc.CRDNumber = vif.individualCRDNumber
join RegulatoryAgency.dbo.Individual i on i.CRDNumber = vif.individualCRDNumber
join #tempSFFirms SFPFirm on SFPFIRM.Firm_CRD__c = cast(vif.firmCRDNumber as varchar(15)) 
where vif.EndDate is null
and RecordType in ('RIA', 'Dually Registered')
and SFPFirm.Does_Not_Require_Selling_Agreement__c = 0
) prep
left join SalesForce_Reporting.[SF_FULLCOPY].vContactRecordType crt on crt.name = prep.RecordType
left join #tempValidLicense cvl on cvl.CRDNumber = prep.individualCRDNumber
left join RegulatoryAgency.dbo.vIndividualLicense_Flattened vilf on vilf.CRDNumber = prep.individualCRDNumber
left join #tempvContactsWithCRD SF_Contact on SF_Contact.CRD__c = prep.individualCRDNumber
where SF_Contact.id is null

END
GO
GRANT EXECUTE ON  [SF_FullCopy].[PrioritizedFirmsContactsToInsert] TO [ETL_User]
GO
