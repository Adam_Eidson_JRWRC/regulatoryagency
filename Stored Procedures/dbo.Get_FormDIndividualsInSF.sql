SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 4/30/2020
-- Description:	Returns individuals that are on Form D AND exist in SalesForce
-- =============================================
--exec RegulatoryAgency.dbo.Get_FormDIndividualsInSF
CREATE PROCEDURE [dbo].[Get_FormDIndividualsInSF]
	
AS
BEGIN

	/****** Script for SelectTopNRows command from SSMS  ******/
select distinct CRDNumber 
into #temp
FROM [RegulatoryAgency].[SEC].[vQualifiedFormD_IndividualData] v

SELECT  distinct v.[CRDNumber]--, 'https://cs15.salesforce.com/'+sf.id
  FROM #temp v
  join openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, CRD__c from Contact where CRD__c != ''''') sf on sf.CRD__c = v.CRDNumber

END
GO
GRANT EXECUTE ON  [dbo].[Get_FormDIndividualsInSF] TO [ETL_User]
GO
