SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/6/2020
-- Description:	Returns new Contact basic info data.
-- =============================================
--exec RegulatoryAgency.SF_FULLCOPY.UpdatedContactBasicInfo
CREATE PROCEDURE [SF_FullCopy].[UpdatedContactBasicInfo]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  /*
-----------------------------
-----------------------------
Basic Info Data
-----------------------------
-----------------------------
*/
DECLARE @from_lsn binary (10) ,@to_lsn binary (10)
DECLARE @DateBeganInIndustryPosition INT
DECLARE @FirstNamePosition INT
DECLARE @LastNamePosition INT
DECLARE @MiddleNamePosition INT
DECLARE @SuffixPosition INT
DECLARE @FINRAStatusPosition INT
DECLARE @SECStatusPosition INT
DECLARE @SECLinkPosition INT
DECLARE @FINRALinkPosition  INT

SET @from_lsn = sys.fn_cdc_map_time_to_lsn('smallest greater than',DATEADD(day,-15,GETDATE()))--sys.fn_cdc_get_min_lsn('dbo_Individual')
SET @to_lsn = sys.fn_cdc_get_max_lsn()
SET @DateBeganInIndustryPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 
'DateBeganInIndustry')
SET @FirstNamePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 
'FirstName')
SET @MiddleNamePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 
'MiddleName')
SET @LastNamePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 
'LastName')
SET @SuffixPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 'Suffix')
SET @FINRAStatusPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 'FINRAStatus')
SET @SECStatusPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 'SECStatus')
SET @SECLinkPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 'SECLink')
SET @FINRALinkPosition = sys.fn_cdc_get_column_ordinal('dbo_Individual', 'FINRALink')

SELECT fn_cdc_get_all_changes_dbo_Individual.__$start_lsn
	,fn_cdc_get_all_changes_dbo_Individual.CRDNumber
	,fn_cdc_get_all_changes_dbo_Individual.__$operation
	,fn_cdc_get_all_changes_dbo_Individual.__$update_mask
	,sys.fn_cdc_is_bit_set(@DateBeganInIndustryPosition, __$update_mask) as 
'DateBeganInIndustry'
	,sys.fn_cdc_is_bit_set(@FirstNamePosition, __$update_mask) as 
'FirstName'
	,sys.fn_cdc_is_bit_set(@MiddleNamePosition, __$update_mask) as 
'MiddleName'
,sys.fn_cdc_is_bit_set(@LastNamePosition, __$update_mask) as 
'LastName'
	,sys.fn_cdc_is_bit_set(@SuffixPosition , __$update_mask) as 
'Suffix'	
,sys.fn_cdc_is_bit_set(@FINRAStatusPosition , __$update_mask) as 
'FINRAStatus'	
,sys.fn_cdc_is_bit_set(@SECStatusPosition , __$update_mask) as 
'SECStatus'	
,sys.fn_cdc_is_bit_set(@SECLinkPosition , __$update_mask) as 
'SECLink'	
,sys.fn_cdc_is_bit_set(@FINRALinkPosition , __$update_mask) as 
'FINRALink'	
into #tempContactChangeBitMap
FROM cdc.fn_cdc_get_all_changes_dbo_Individual(@from_lsn, @to_lsn, 'all')
where __$operation = 4 --only grab updates. We are taking care of inserts another way.
ORDER BY __$seqval

--drop table #tempContactChangeBitMap

--Holding off on this till CRD Validation field is implemented. AE 8/21/2020
--Get any Contacts with CRD # populated but no FINRA URL populated
--and insert them into #tempContactChangeBitMap with all values set to 1
--insert into #tempContactChangeBitMap
--(
--__$start_lsn,
--CRDNumber,
--__$operation,
--__$update_Mask,
--DateBeganInIndustry,
--FirstName,
--MiddleName,
--LastName,
--Suffix,
--FINRAStatus,
--SECStatus,
--SECLink,
--FINRALink
--)
--Select 0 as __$start_lsn,
--CRD__c as CRDNumber ,
--4 as __$operation,
--0 as __$update_Mask,
--cast('True' as bit) AS 'DateBeganInIndustry'
--	,cast('False' as bit) as 
--'FirstName'
--	,cast('False' as bit) as 
--'MiddleName'
--,cast('False' as bit) as 
--'LastName'
--	,cast('False' as bit) as 
--'Suffix'	
--,cast('True' as bit) as 
--'FINRAStatus'	
--,cast('True' as bit) as 
--'SECStatus'	
--,cast('True' as bit) as 
--'SECLink'	
--,cast('True' as bit) as 
--'FINRALink'
--select *
--From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, CRD__c from Contact where CRD__c is not null and FINRA_Detailed_Report_URL__c is null')

select prep.id,
Date_Became_Rep__c,
dbo.ProperCase(FirstName) as FirstName,
dbo.ProperCase(MiddleName) as MiddleName,
dbo.ProperCase(LastName) as LastName,
dbo.ProperCase(Suffix) as Suffix,
FINRALink as FINRA_Detailed_Report_URL__c,
SECLink,
crt.id as RecordTypeId 
from (
select distinct SF_Contact.id,
case when t.DateBeganInIndustry = 1 and i.dateBeganInIndustry is not null
then i.dateBeganInIndustry
end as Date_Became_Rep__c,
case when t.FirstName = 1
then i.FirstName
end as FirstName,
case when t.MiddleName = 1
then i.MiddleName
end as MiddleName,
case when t.LastName = 1
then i.LastName
end as LastName,
case when t.Suffix = 1
then i.Suffix
end as Suffix,
case when t.FINRAStatus = 1
or t.SECStatus = 1
then RegulatoryAgency.dbo.[ContactRecordType](i.FINRAStatus, i.SECStatus)
end as RecordType,
case when t.FINRAStatus = 1
or t.SECStatus = 1
then RegulatoryAgency.dbo.[ContactFINRAStatus](i.FINRAStatus, i.SECStatus)
end as FINRA_Status__c,
case when t.FINRALink = 1
then i.FINRALink
end as FINRALink,
case when t.SECLink = 1
then i.SECLink
end as SECLink
From #tempContactChangeBitMap t 
join RegulatoryAgency.dbo.Individual i on t.CRDNumber = i.CRDNumber
join SalesForce_reporting.SF_FULLCOPY.vContactsWithCRD SF_Contact on SF_Contact.CRD__c = i.CRDNumber) prep
left join SalesForce_Reporting.SF_FULLCOPY.vContactRecordType crt on crt.name = prep.RecordType
For XML Raw ('Contact'), ROOT ('Contacts')


END
GO
GRANT EXECUTE ON  [SF_FullCopy].[UpdatedContactBasicInfo] TO [ETL_User]
GO
