SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 9/12/2020
-- Description:	Reconciles RegulatoryAgency records with SF that do not exist, but should.
-- =============================================
CREATE PROCEDURE [dbo].[ReconcileMissingRegulatoryAgency]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   --drop table #tempBDRIAToCreate
--Audit BD/RIA Associations

--Get all BD/RIA Associations from SF where CRD's are not null and BD/RIA Association record type is FINRA/SEC
  with cte_SF_BDRIA as (
  select distinct vcd.CRD__c,
vbd.Firm_CRD__c, active__c,
vcd.id as contact__c,
vbd.id as broker_Dealer__c,
bdria.id as BDRIAAssociationId
From SalesForce_Reporting.dbo.vContactsWithCRD vcd
join SalesForce_Reporting.dbo.vBD_RIA_Association bdria on vcd.id = bdria.Contact__c
join SalesForce_Reporting.dbo.vBrokerDealer vbd on  vbd.id = bdria.Broker_Dealer__c
where vcd.CRD__c is not null
and vbd.firm_CRD__c is not null
), cte_RegAgency_BDRIA as ( --Get all active BD/RIA Associations from Regulatory Agency DB where Contact exists in SF
SELECT vif.[FirstName]
      ,vif.[LastName]
      ,[individualCRDNumber]
      ,[IndividualFinraStatus]
      ,[IndividualSECStatus]
      ,[firmName]
      ,[firmCRDNumber]
      ,[FirmFINRAStatus]
      ,[FirmSECStatus]
      ,[StartDate]
      ,[EndDate]
  FROM [RegulatoryAgency].[dbo].[vIndividualFirm] vif
  join SalesForce_Reporting.dbo.vContactsWithCRD SFContact on SFContact.CRD__c = vif.individualCRDNumber
  where (FirmFINRAStatus = 'Active'
  or FirmSECStatus = 'Active')
  and (IndividualFinraStatus = 'Active'
  or  IndividualSECStatus = 'Active')
  and endDate is null
  )

  --Get BD/RIA Associations that should be created.
  select distinct ra.individualCRDNumber,
  ra.firmCRDNumber
  into #tempBDRIAToCreate
  From cte_RegAgency_BDRIA ra
  left join cte_SF_BDRIA sf on ra.individualCRDNumber = sf.CRD__c
								and sf.Firm_CRD__c = ra.firmCRDNumber
  where crd__c is null
		--or active__c = 0

	select vccrd.id as Contact__c,
	vbd.id as Broker_Dealer__c,
	'true' as Active__c,
	bdriart.id as recordType
	from #tempBDRIAToCreate cbac
	join SalesForce_Reporting.dbo.vContactsWithCRD vccrd on vccrd.CRD__c = cbac.individualCRDNumber
	join SalesForce_Reporting.dbo.vBrokerDealer vbd on vbd.Firm_CRD__c = cbac.firmCRDNumber
	cross join SalesForce_Reporting.dbo.vBDRIAAssociationRecordType bdriart where bdriart.name = 'FINRA/SEC'
	


END
GO
