SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 3/26/2020
-- Description:	Returns Differences between SalesForce Live and Regulatory Agency data.
-- =============================================
--exec RegulatoryAgency.[dbo].[RPT_SalesForceDifference] 
CREATE PROCEDURE [dbo].[RPT_SalesForceDifference] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
select *
into #tempSF
From vFINRASECDiff_SFData sf


Select distinct sf.CRD__c,
sf.id as [Salesforce URL],
sf.ColumnName as [SalesForce Field Name],
sf.ColumnValue as [SalesForce Field Value],
wh.ColumnValue as [Proposed Field Value]
From #tempSF sf
join vFINRASECDiff_WarehouseData wh on wh.CRDNumber = sf.CRD__c
										and wh.ColumnName = sf.ColumnName

where sf.ColumnValue != wh.ColumnValue
and sf.ColumnName not like 'Mailing%'
and sf.columnName != 'Phone'
--and wh.crdNumber = '4504744'
--and wh.ColumnName = 'Date_Became_Rep__c'--'FINRA_Status__c'
--and sf.ColumnValue = ''
union 
Select distinct sf.CRD__c,
sf.id as [Salesforce URL],
sf.ColumnName as [SalesForce Field Name],
sf.ColumnValue as [SalesForce Field Value],
wh.ColumnValue as [Proposed Field Value]
From #tempSF sf
join vFINRASECDiff_WarehouseData wh on wh.CRDNumber = sf.CRD__c
										and wh.ColumnName = sf.ColumnName

where sf.ColumnValue != wh.ColumnValue
and sf.columnName = 'Phone'
and case when sf.columnValue = ''
then 1
else 0
end = 1
order by sf.crd__c
END
GO
GRANT EXECUTE ON  [dbo].[RPT_SalesForceDifference] TO [Report_User]
GO
