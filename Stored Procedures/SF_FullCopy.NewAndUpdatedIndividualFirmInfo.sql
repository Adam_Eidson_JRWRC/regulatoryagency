SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/6/2020
-- Description:	Returns Individual ID's, Firm ID's, and EndDates that have been inserted or updated within the past five days. 
-- This is used to insert BD/RIA Association records as well as update existing records when an individual leaves a firm.
-- =============================================
--exec RegulatoryAgency.SF_FULLCOPY.NewAndUpdatedIndividualFirmInfo
CREATE PROCEDURE [SF_FullCopy].[NewAndUpdatedIndividualFirmInfo]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @NewBDRIAAssociations XML
	DECLARE @UpdateBDRIAAssociations XML
	declare @FinraSEC_RecordType varchar(50)
  
  --Populate Contact table
  select distinct CRD__c, id
  into #TempSFContact
  From SalesForce_Reporting.SF_FULLCOPY.vContactsWithCRD

  --populate BD table
  select distinct Firm_CRD__c, id
  into #TempSFBD
  From SalesForce_Reporting.SF_FULLCOPY.vDistinctFirmCRD

  --populate BD/RIA Association table
  select distinct id, Broker_Dealer__c, Contact__c
  into #TempSFBDRIAAssociation
  From SalesForce_Reporting.SF_FULLCOPY.vBD_RIA_Association
  
  --get FINRA/SEC record Type Id
  select @FinraSEC_RecordType = id
  from SalesForce_Reporting.dbo.vBDRIAAssociationRecordType 
  where name = 'FINRA/SEC'

  --get ANY missing BD/RIA associations, regardless of creation/active date.
Declare @ReconcileMissingRegulatoryAgency  TABLE (
Contact__c varchar(50),
Broker_Dealer__c varchar(50),
Active__c varchar(5),
recordType varchar(50)

) 
insert into @ReconcileMissingRegulatoryAgency  
EXEC RegulatoryAgency.SF_FULLCOPY.ReconcileMissingRegulatoryAgency


/*
-----------------------------
-----------------------------
Actively Registered Firm Data
-----------------------------
-----------------------------
*/
DECLARE @from_lsn binary (10) ,@to_lsn binary (10)
DECLARE @EndDatePosition INT
DECLARE @StartDatePosition INT

SET @from_lsn = sys.fn_cdc_map_time_to_lsn('smallest greater than',DATEADD(day,-15,GETDATE()))--sys.fn_cdc_get_min_lsn('dbo_Individual')
SET @to_lsn = sys.fn_cdc_get_max_lsn()
SET @EndDatePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual_Firm', 
'EndDate')
SET @StartDatePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual_Firm', 
'StartDate')

SELECT fn_cdc_get_all_changes_dbo_Individual_Firm.__$start_lsn
	,fn_cdc_get_all_changes_dbo_Individual_Firm.id
	,fn_cdc_get_all_changes_dbo_Individual_Firm.__$operation
	,fn_cdc_get_all_changes_dbo_Individual_Firm.__$update_mask
	,sys.fn_cdc_is_bit_set(@EndDatePosition, __$update_mask) as 
'EndDate'
,sys.fn_cdc_is_bit_set(@StartDatePosition, __$update_mask) as 
'StartDate'
into #tempIndividualFirmChangeBitMap
FROM cdc.fn_cdc_get_all_changes_dbo_Individual_Firm(@from_lsn, @to_lsn, 'all')
where __$operation in  (2,4)
ORDER BY __$seqval

;with cte_NewBDRIAPrep as (
select distinct 
sfContact.id as Contact__c,
sfBD.id as Broker_Dealer__c,
'true' as Active__c,
@FinraSEC_RecordType as RecordTypeId
--iff.StartDate, --will be adding into data load in the future
--iff.EndDate --will be adding into data load in the future
From RegulatoryAgency.dbo.Individual_Firm iff
join RegulatoryAgency.dbo.individual i on i.IndividualId = iff.IndividualId
join RegulatoryAgency.dbo.Firm f on f.FirmId = iff.FirmId
join #TempSFContact sfContact on  sfContact.CRD__c = convert(varchar(15),i.CRDNumber)
join #TempSFBD sfBD on sfBD.Firm_CRD__c = convert(varchar(15),f.FINRANumber)
join #tempIndividualFirmChangeBitMap t on t.id = iff.id
left join #TempSFBDRIAAssociation SFBDRIAAssociation on SFBDRIAAssociation.Contact__c = sfContact.id	
														and SFBDRIAAssociation.Broker_Dealer__c = sfBD.id
where t.__$operation = 2
and iff.EndDate is null
and SFBDRIAAssociation.id is null --Check if this record already exists!
union 
select distinct 
vc.id as Contact__c,
vbd.id as Broker_Dealer__c,
'true' as Active__c,
@FinraSEC_RecordType as RecordTypeId
From RegulatoryAgency.dbo.vIndividualFirm vif
join SalesForce_Reporting.SF_FULLCOPY.vDistinctFirmCRD vbd on cast(vbd.Firm_CRD__c as varchar(50)) = vif.firmCRDNumber
join SalesForce_Reporting.SF_FULLCOPY.vContactsWithCRD vc on vc.CRD__c = vif.individualCRDNumber
left join SalesForce_Reporting.SF_FULLCOPY.vBD_RIA_Association vbdria on vbdria.Broker_Dealer__c = vbd.id
																	and vbdria.Contact__c = vc.id

where vbd.Firm_CRD__c not like '%fake%'
and vbdria.id is null
and vif.EndDate is null --still active
), cte_finalNewBDRIAAssociation as (

select *
from cte_NewBDRIAPrep
union
select *
From @ReconcileMissingRegulatoryAgency  
)


select @NewBDRIAAssociations = (
select *
From cte_finalNewBDRIAAssociation
For XML Raw ('BD_RIA_Association__c'), ROOT ('NewBD_RIA_Associations')
)

select @UpdateBDRIAAssociations = (
select distinct SFBDRIAAssociation.id
--case when t.StartDate = 1 
--then iff.StartDate
--end as StartDate, --will be adding into data load in the future
--case when t.EndDate = 1 
--then iff.EndDate
--end as EndDate --will be adding into data load in the future
From RegulatoryAgency.dbo.Individual_Firm iff
join RegulatoryAgency.dbo.individual i on i.IndividualId = iff.IndividualId
join RegulatoryAgency.dbo.Firm f on f.FirmId = iff.FirmId
join #TempSFContact sfContact on  sfContact.CRD__c = convert(varchar(15),i.CRDNumber)
join #TempSFBD sfBD on sfBD.Firm_CRD__c = convert(varchar(15),f.FINRANumber)
join #TempSFBDRIAAssociation SFBDRIAAssociation on SFBDRIAAssociation.Broker_Dealer__c = sfBD.id
													and SFBDRIAAssociation.Contact__c = sfContact.id
join #tempIndividualFirmChangeBitMap t on t.id = iff.id
where t.__$operation = 4

For XML Raw ('BD_RIA_Association__c'), ROOT ('UpdateBD_RIA_Associations')
)

select CONVERT(XML,CONCAT(ISNULL(CONVERT(varchar(MAX),@NewBDRIAAssociations),''), ISNULL(CONVERT(varchar(max),@UpdateBDRIAAssociations),''))) as Results
FOR XML RAW, ROOT ('BD_RIA_ASSOCIATION')

END
GO
GRANT EXECUTE ON  [SF_FullCopy].[NewAndUpdatedIndividualFirmInfo] TO [ETL_User]
GO
