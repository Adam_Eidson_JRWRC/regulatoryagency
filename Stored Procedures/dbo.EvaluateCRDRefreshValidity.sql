SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/15/2020
-- Description:	Evaluate if CRD should be queried against FINRA/SEC or not
-- =============================================
--exec RegulatoryAgency.dbo.[EvaluateCRDRefreshValidity] @CRD = 6414463, @ObjectType = 'Individual', @RefreshInterval = 3
CREATE PROCEDURE [dbo].[EvaluateCRDRefreshValidity]
	-- Add the parameters for the stored procedure here
	@CRD bigint,
	@ObjectType varchar(12),
	@RefreshInterval int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @QueryCRD int = 1


	if  exists(select CRD 
			from RegulatoryAgency.dbo.CRDRefreshTracking 
			where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
			and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType)
			and LastQuery> DATEADD(day,-@RefreshInterval,GETDATE())
			)
			BEGIN

				SELECT @QueryCRD = 0
			END
	
		select @QueryCRD as result
	

	
END
GO
GRANT EXECUTE ON  [dbo].[EvaluateCRDRefreshValidity] TO [ETL_User]
GO
