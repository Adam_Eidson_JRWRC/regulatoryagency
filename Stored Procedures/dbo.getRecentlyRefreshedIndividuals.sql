SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 6/30/2020
-- Description:	Returns records that were refreshed between now and the @DaysSinceRefresh parameter
-- =============================================
CREATE PROCEDURE [dbo].[getRecentlyRefreshedIndividuals]
	@DaysSinceRefresh int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select CRD
	from RegulatoryAgency.dbo.CRDRefreshTracking
	where LastQuery > DATEADD(day,-@DaysSinceRefresh, GETDATE())

END
GO
