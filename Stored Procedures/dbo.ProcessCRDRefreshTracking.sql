SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 6/30/2020
-- Description:	Process CRD to insert or update the LastQuery column so we can later batch requests to FINRA/SEC
-- =============================================
CREATE PROCEDURE [dbo].[ProcessCRDRefreshTracking]
	-- Add the parameters for the stored procedure here
	@CRD bigint,
	@ObjectType varchar(12)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Test if CRD exists
	if not exists(select CRD 
			from RegulatoryAgency.dbo.CRDRefreshTracking 
			where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
			--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 
			)
		BEGIN
		
			--if not, insert
			insert into RegulatoryAgency.dbo.CRDRefreshTracking 
			(
			CRD, 
			ObjectType
			)

			select @CRD,
			@ObjectType

		END

	ELSE 
		BEGIN
		--else, update
		update crt
		set LastQuery = GETDATE()
		from RegulatoryAgency.dbo.CRDRefreshTracking crt
		where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
		--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 

		select *
		from RegulatoryAgency.dbo.CRDRefreshTracking crt
		where convert(varchar(20), CRD) = convert(varchar(20),@CRD)
		--and convert(varchar(12), ObjectType )= convert(varchar(12),@ObjectType) 

		END

	

	
END
GO
GRANT EXECUTE ON  [dbo].[ProcessCRDRefreshTracking] TO [ETL_User]
GO
