SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/8/2020
-- Description:	Returns only records that have recorded a firm change in the past 7 days and may need their Prior Firm Name field to be updated in SF.
-- =============================================
--exec dbo.UpdateContactPriorFirm
CREATE PROCEDURE [dbo].[UpdateContactPriorFirm]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
	  --Populate Contact table
	  select distinct CRD__c, id
	  into #TempSFContact
	  From SalesForce_Reporting.dbo.vContactsWithCRD

	/*
	-----------------------------
	-----------------------------
	Actively Registered Firm Data
	-----------------------------
	-----------------------------
	*/
	DECLARE @from_lsn binary (10) ,@to_lsn binary (10)
	DECLARE @EndDatePosition INT
	DECLARE @StartDatePosition INT

	SET @from_lsn = sys.fn_cdc_map_time_to_lsn('smallest greater than',DATEADD(day,-1,GETDATE()))--sys.fn_cdc_get_min_lsn('dbo_Individual')
	SET @to_lsn = sys.fn_cdc_get_max_lsn()
	SET @EndDatePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual_Firm', 
	'EndDate')
	SET @StartDatePosition = sys.fn_cdc_get_column_ordinal('dbo_Individual_Firm', 
	'StartDate')

	SELECT fn_cdc_get_all_changes_dbo_Individual_Firm.__$start_lsn
		,fn_cdc_get_all_changes_dbo_Individual_Firm.id
		,fn_cdc_get_all_changes_dbo_Individual_Firm.__$operation
		,fn_cdc_get_all_changes_dbo_Individual_Firm.__$update_mask
		,sys.fn_cdc_is_bit_set(@EndDatePosition, __$update_mask) as 
	'EndDate'
	,sys.fn_cdc_is_bit_set(@StartDatePosition, __$update_mask) as 
	'StartDate'
	into #tempIndividualFirmChangeBitMap
	FROM cdc.fn_cdc_get_all_changes_dbo_Individual_Firm(@from_lsn, @to_lsn, 'all')
	where __$operation = 4
	ORDER BY __$seqval


	
select distinct 
sfContact.id,
vipf.PriorFirmName as Prior_Firm_Name__c
from #tempIndividualFirmChangeBitMap t
join RegulatoryAgency.dbo.Individual_Firm iff on t.id = iff.id
join RegulatoryAgency.dbo.individual i on i.IndividualId = iff.IndividualId
join #TempSFContact sfContact on  sfContact.CRD__c =  convert(varchar(15),i.CRDNumber)
left join RegulatoryAgency.dbo.vIndividualPriorFirm vipf on vipf.individualId = iff.IndividualId

END
GO
GRANT EXECUTE ON  [dbo].[UpdateContactPriorFirm] TO [ETL_User]
GO
