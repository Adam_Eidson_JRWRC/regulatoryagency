SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/8/2020
-- Description:	Scalar function to return Contact Record Type
-- =============================================
CREATE FUNCTION [dbo].[ContactFINRAStatus] 
(
	@FINRAStatus varchar(25),
	@SECStatus varchar(25)

)
RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ContactFINRAStatus varchar(20)

	select @ContactFINRAStatus = 
	case when @FINRAStatus = 'Active'
	or @SECStatus = 'Active'
	then 'Active'
	when @FINRAStatus != 'Active'
	and @SECStatus != 'Active'
	then 'Inactive'
	else Null
	end
	-- Return the result of the function
	RETURN @ContactFINRAStatus

END
GO
