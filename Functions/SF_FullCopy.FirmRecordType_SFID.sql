SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/8/2020
-- Description:	Scalar function to return Firm Record Type id
-- =============================================
CREATE FUNCTION [SF_FullCopy].[FirmRecordType_SFID] 
(
	@FINRAStatus varchar(25),
	@SECStatus varchar(25)

)
RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @RecordType varchar(20),
	@ReturnRecordType varchar(30)

	select @RecordType = 
	case when isNull(@FINRAStatus,'') = 'Active'
	and isNull(@SECStatus, '') != 'Active'
	then 'Broker-Dealer'
	when isNull(@FINRAStatus, '') != 'Active'
	and isNull(@SECStatus, '') = 'Active'
	then 'RIA'
	when isNull(@FINRAStatus, '') = 'Active'
	and isNull(@SECStatus, '') = 'Active'
	then 'Broker-Dealer/RIA'
	end
	
	select @ReturnRecordType =  rt.id
	from SalesForce_Reporting.SF_FULLCOPY.vFirmRecordType rt
	where rt.name = @RecordType

	-- Return the result of the function
	RETURN @ReturnRecordType

END
GO
