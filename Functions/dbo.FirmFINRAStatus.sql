SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/9/2020
-- Description:	Scalar function to return Firm FINRA Status
-- =============================================
CREATE FUNCTION [dbo].[FirmFINRAStatus] 
(
	@FINRAStatus varchar(25),
	@SECStatus varchar(25)

)
RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @FirmFINRAStatus varchar(20)

	select @FirmFINRAStatus = 
	case when isNULL(@FINRAStatus, '') = 'Active'
	or ISNULL(@SECStatus,'') = 'Active'
	then 'Active'
	when ISNULL(@FINRAStatus,'') != 'Active'
	and ISNULL(@SECStatus,'') != 'Active'
	then 'Inactive'
	else Null
	end
	-- Return the result of the function
	RETURN @FirmFINRAStatus

END
GO
