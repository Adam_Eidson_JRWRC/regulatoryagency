SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/6/2020
-- Description:	Scalar function to return Contact Record Type id
-- =============================================
CREATE FUNCTION [dbo].[ContactRecordType_SFID] 
(
	@FINRAStatus varchar(25),
	@SECStatus varchar(25)

)
RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @RecordType varchar(20),
	@ReturnRecordType varchar(30)

	select @RecordType = 
	case when @FINRAStatus = 'Active'
	and @SECStatus != 'Active'
	then 'Broker-Dealer'
	when @FINRAStatus != 'Active'
	and @SECStatus = 'Active'
	then 'RIA'
	when @FINRAStatus = 'Active'
	and @SECStatus = 'Active'
	then 'Dually Registered'
	else Null
	end
	
	select @ReturnRecordType =  rt.id
	from SalesForce_Reporting.dbo.vContactRecordType rt
	where rt.name = @RecordType

	-- Return the result of the function
	RETURN @ReturnRecordType

END
GO
