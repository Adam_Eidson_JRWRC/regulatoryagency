ALTER ROLE [db_ddladmin] ADD MEMBER [ETL_User]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [JRW\bgolly]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [JRW\etl_user]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Report_User]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Tableau_User]
GO
