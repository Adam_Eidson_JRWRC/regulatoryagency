CREATE TABLE [dbo].[CRDRefreshTracking]
(
[CRD] [bigint] NOT NULL,
[ObjectType] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastQuery] [datetime] NOT NULL CONSTRAINT [DF_CRDRefreshTracking_LastQuery] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CRDRefreshTracking] ADD CONSTRAINT [PK__CRDRefre__C1F887F5EDA3975D] PRIMARY KEY CLUSTERED ([CRD]) ON [PRIMARY]
GO
