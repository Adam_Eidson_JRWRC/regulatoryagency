CREATE TABLE [SEC].[FirmWebsite]
(
[WebsiteId] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[Website] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_FirmWebsite_CreatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
