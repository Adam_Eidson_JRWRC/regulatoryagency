CREATE TABLE [STAGE].[Individual_License]
(
[IndividualCRD] [int] NOT NULL,
[DateAwarded] [date] NOT NULL,
[LicenseName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LicenseDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
