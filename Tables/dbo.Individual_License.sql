CREATE TABLE [dbo].[Individual_License]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[LicenseId] [int] NOT NULL,
[IndividualId] [int] NOT NULL,
[LicenseDate] [date] NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_License_CreatedAt] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_License] ADD CONSTRAINT [PK__Individu__3214EC075A9106F2] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Individual_License_12_1429580131__K3_K2] ON [dbo].[Individual_License] ([IndividualId], [LicenseId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_License] ADD CONSTRAINT [FK_Individual_License_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_License] ADD CONSTRAINT [FK_Individual_License_License] FOREIGN KEY ([LicenseId]) REFERENCES [dbo].[License] ([LicenseId]) ON DELETE CASCADE
GO
