CREATE TABLE [dbo].[Firm]
(
[FirmId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRANumber] [int] NULL,
[CreatedAt] [datetime] NULL,
[updatedAt] [datetime] NULL,
[FINRAStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighNetWorthAuM] [decimal] (20, 2) NULL,
[NumberOfHighNetWorthClients] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm] ADD CONSTRAINT [PK__Firm__1F1F209C4F29982A] PRIMARY KEY CLUSTERED ([FirmId]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1301579675_1_4] ON [dbo].[Firm] ([FirmId], [FINRANumber])
GO
