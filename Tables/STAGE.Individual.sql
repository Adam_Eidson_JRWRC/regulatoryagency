CREATE TABLE [STAGE].[Individual]
(
[CRDNumber] [int] NULL,
[SECNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECLink] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRALink] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRAStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateBeganInIndustry] [date] NULL
) ON [PRIMARY]
GO
