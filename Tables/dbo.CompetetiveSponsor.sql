CREATE TABLE [dbo].[CompetetiveSponsor]
(
[CompetetiveSponsorId] [int] NOT NULL IDENTITY(1, 1),
[FirmCRDNumber] [int] NOT NULL,
[CommonName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
