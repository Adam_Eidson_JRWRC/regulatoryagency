CREATE TABLE [dbo].[Individual_BranchLocation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[postalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocatedAtFlag] [bit] NULL,
[SupervisedFromFlag] [bit] NULL,
[PrivateResidenceFlag] [bit] NULL,
[geolocation] [sys].[geography] NULL,
[latitude] [float] NULL,
[longitude] [float] NULL,
[BranchOfficeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [PK__Individu__3214EC07FB03EA4A] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_274100017_2_3_4_5_6_8_7] ON [dbo].[Individual_BranchLocation] ([FirmId], [IndividualId], [Street1], [Street2], [City], [postalCode], [State])
GO
CREATE STATISTICS [_dta_stat_274100017_4_5_6_8_7] ON [dbo].[Individual_BranchLocation] ([Street1], [Street2], [City], [postalCode], [State])
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [FK_Individual_BranchLocation_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_BranchLocation] ADD CONSTRAINT [FK_Individual_BranchLocation_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
