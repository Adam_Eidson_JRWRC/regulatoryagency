CREATE TABLE [SEC].[Firm]
(
[FirmId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRDNumber] [int] NULL,
[CreatedAt] [datetime] NULL,
[updatedAt] [datetime] NULL,
[SECStatus] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighNetWorthAuM] [decimal] (20, 2) NULL,
[NumberOfHighNetWorthClients] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [SEC].[Firm] ADD CONSTRAINT [PK__Firm__1F1F209C19FCB7FB] PRIMARY KEY CLUSTERED ([FirmId]) ON [PRIMARY]
GO
