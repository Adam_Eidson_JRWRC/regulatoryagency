CREATE TABLE [FINRA].[DirectOwner]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NULL,
[IndividualCRD] [int] NULL,
[Name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Role] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [FINRA].[DirectOwner] ADD CONSTRAINT [PK__DirectOw__3214EC07BB0D8A36] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1618104805_4_2_6] ON [FINRA].[DirectOwner] ([IndividualCRD], [FirmId], [Role])
GO
ALTER TABLE [FINRA].[DirectOwner] ADD CONSTRAINT [FK_DirectOwner_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [FINRA].[DirectOwner] ADD CONSTRAINT [FK_DirectOwner_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
