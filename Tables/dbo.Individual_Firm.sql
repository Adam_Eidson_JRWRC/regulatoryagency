CREATE TABLE [dbo].[Individual_Firm]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[IndividualId] [int] NOT NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual_Firm] ADD CONSTRAINT [PK__Individu__3214EC070C4DB676] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IF_EndDate_FirmId_IndId] ON [dbo].[Individual_Firm] ([EndDate]) INCLUDE ([FirmId], [IndividualId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IndId_EndDate_FId] ON [dbo].[Individual_Firm] ([IndividualId], [EndDate]) INCLUDE ([FirmId]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1330103779_2_5] ON [dbo].[Individual_Firm] ([FirmId], [EndDate])
GO
CREATE STATISTICS [_dta_stat_1330103779_2_1_5] ON [dbo].[Individual_Firm] ([FirmId], [Id], [EndDate])
GO
CREATE STATISTICS [_dta_stat_1330103779_2_3] ON [dbo].[Individual_Firm] ([FirmId], [IndividualId])
GO
CREATE STATISTICS [_dta_stat_1330103779_3_5_2_1] ON [dbo].[Individual_Firm] ([IndividualId], [EndDate], [FirmId], [Id])
GO
ALTER TABLE [dbo].[Individual_Firm] WITH NOCHECK ADD CONSTRAINT [FK_Individual_Firm_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_Firm] WITH NOCHECK ADD CONSTRAINT [FK_Individual_Firm_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([IndividualId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Individual_Firm] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)
GO
