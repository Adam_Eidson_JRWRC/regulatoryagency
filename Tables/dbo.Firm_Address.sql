CREATE TABLE [dbo].[Firm_Address]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[FirmId] [int] NOT NULL,
[Street1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Faxnumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MainAddress] [bit] NULL,
[MailingAddress] [bit] NULL,
[County] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isIAAddress] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm_Address] ADD CONSTRAINT [PK__Firm_Add__3214EC07BE3B64F9] PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_34099162_2_14_3_4_5_9_6_7_8_13_11] ON [dbo].[Firm_Address] ([FirmId], [isIAAddress], [Street1], [Street2], [City], [PhoneNumber], [State], [PostalCode], [Country], [County], [MainAddress])
GO
CREATE STATISTICS [_dta_stat_34099162_9_3_4] ON [dbo].[Firm_Address] ([PhoneNumber], [Street1], [Street2])
GO
CREATE STATISTICS [_dta_stat_34099162_3_4_5_9_6_7_8_13_11_14] ON [dbo].[Firm_Address] ([Street1], [Street2], [City], [PhoneNumber], [State], [PostalCode], [Country], [County], [MainAddress], [isIAAddress])
GO
ALTER TABLE [dbo].[Firm_Address] ADD CONSTRAINT [FK_Firm_Address_Firm] FOREIGN KEY ([FirmId]) REFERENCES [dbo].[Firm] ([FirmId]) ON DELETE CASCADE
GO
