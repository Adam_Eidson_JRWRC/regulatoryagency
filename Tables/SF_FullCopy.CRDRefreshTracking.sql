CREATE TABLE [SF_FullCopy].[CRDRefreshTracking]
(
[CRD] [bigint] NOT NULL,
[ObjectType] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastQuery] [datetime] NOT NULL CONSTRAINT [DF_CRDRefreshTracking_LastQuery] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [SF_FullCopy].[CRDRefreshTracking] ADD CONSTRAINT [PK__CRDRefre__C1F887F55587B933] PRIMARY KEY CLUSTERED ([CRD]) ON [PRIMARY]
GO
