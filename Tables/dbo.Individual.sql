CREATE TABLE [dbo].[Individual]
(
[IndividualId] [int] NOT NULL IDENTITY(1, 1),
[CRDNumber] [int] NULL,
[SECNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECLink] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRALink] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_CreatedAt] DEFAULT (getdate()),
[updatedAt] [datetime] NOT NULL CONSTRAINT [DF_Individual_updatedAt] DEFAULT (getdate()),
[SECStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRAStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateBeganInIndustry] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Individual] ADD CONSTRAINT [PK__Individu__2DA106D6C93AECD4] PRIMARY KEY CLUSTERED ([IndividualId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Individual_12_1333579789__K2] ON [dbo].[Individual] ([CRDNumber]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1333579789_2_1_12_13_14] ON [dbo].[Individual] ([CRDNumber], [IndividualId], [SECStatus], [FINRAStatus], [dateBeganInIndustry])
GO
CREATE STATISTICS [_dta_stat_1333579789_2_12_13_14] ON [dbo].[Individual] ([CRDNumber], [SECStatus], [FINRAStatus], [dateBeganInIndustry])
GO
