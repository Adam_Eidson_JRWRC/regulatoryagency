CREATE TABLE [SEC].[ISSUERS]
(
[IssuersId] [int] NOT NULL IDENTITY(1, 1),
[ACCESSIONNUMBER] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IS_PRIMARYISSUER_FLAG] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUER_SEQ_KEY] [bigint] NOT NULL,
[CIK] [int] NULL,
[ENTITYNAME] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STREET1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STREET2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATEORCOUNTRY] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATEORCOUNTRYDESCRIPTION] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIPCODE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUERPHONENUMBER] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JURISDICTIONOFINC] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUER_PREVIOUSNAME_1] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUER_PREVIOUSNAME_2] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUER_PREVIOUSNAME_3] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EDGAR_PREVIOUSNAME_1] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EDGAR_PREVIOUSNAME_2] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EDGAR_PREVIOUSNAME_3] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENTITYTYPE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENTITYTYPEOTHERDESC] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YEAROFINC_TIMESPAN_CHOICE] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YEAROFINC_VALUE_ENTERED] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [SEC].[ISSUERS] ADD CONSTRAINT [PK__ISSUERS__F66BD695646A27E8] PRIMARY KEY CLUSTERED ([ACCESSIONNUMBER], [ISSUER_SEQ_KEY]) ON [PRIMARY]
GO
