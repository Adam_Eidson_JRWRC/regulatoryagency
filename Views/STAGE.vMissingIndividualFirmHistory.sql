SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [STAGE].[vMissingIndividualFirmHistory] as 
select distinct crdnumber 
from dbo.individual i
left join dbo.Individual_firm iff on iff.IndividualId = i.IndividualId
where iff.id is null
and ISNULL(i.finraStatus , '') <>'NotInScope'
GO
