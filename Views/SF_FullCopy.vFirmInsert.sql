SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE View [SF_FullCopy].[vFirmInsert] as 
with cte_Firms as (
select distinct f.FINRANumber as Firm_CRD__c,
f.Name as Name,
'https://brokercheck.finra.org/firm/summary/'+cast(f.FINRANumber as varchar(15)) as FirmFINRAURL,
f.FINRAStatus as FINRA_Status__c,
--f.SECStatus as SEC_Status__c, --To be leveraged in the future
f.HighNetWorthAuM as Firm_High_Net_Worth_AUM__c,
f.NumberOfHighNetWorthClients as Firm_of_High_Net_Worth_Clients__c,
ffd.totalFormDSalescommissions as Total_Form_D_Sales_Commissions__c
from RegulatoryAgency.dbo.firm f
join RegulatoryAgency.dbo.vIndividualFirm vif on vif.firmCRDNumber = f.finraNumber--records that we know about in the local datastore. Firms and Contacts
join SalesForce_Reporting.SF_FULLCOPY.vContactsWithCRD sfContact on sfContact.CRD__c = vif.individualCRDNumber --contacts that exist in SF.
left join SalesForce_Reporting.SF_FULLCOPY.vDistinctFirmCRD vdfc on convert(varchar(15),vdfc.Firm_CRD__c) = convert(varchar(15),vif.FirmCRDNumber )--firms that exist in SF
left join RegulatoryAgency.[SEC].[vFormD_FirmData] ffd on ffd.FirmCRDNumber = vif.firmCRDNumber
where vdfc.Firm_CRD__c is null
and vif.EndDate is null --still active at firm

)

select distinct cf.*,
STRING_AGG( cast(vfw.website as varchar(max)), ', ') as websites
from cte_Firms cf
left join (select distinct finranumber, website from FINRA.dbo.vFirmWebsite) vfw on vfw.FINRANumber = cf.Firm_CRD__c
group by
Name,
FirmFINRAURL,
Firm_CRD__c,
FINRA_Status__c,
--SEC_Status__c, --To be leveraged in the future
Firm_High_Net_Worth_AUM__c,
Firm_of_High_Net_Worth_Clients__c,
Total_Form_D_Sales_Commissions__c

GO
