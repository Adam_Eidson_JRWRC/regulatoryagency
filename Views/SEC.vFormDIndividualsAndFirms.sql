SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/

create view [SEC].[vFormDIndividualsAndFirms] as
SELECT r.*, minimuminvestmentaccepted
  FROM [RegulatoryAgency].[SEC].[RECIPIENTS] r
  join [RegulatoryAgency].[SEC].FormDSubmission d on d.AccessionNumber  = r.accessionNumber
  join [RegulatoryAgency].[SEC].offering o on o.accessionnumber = d.accessionnumber
  join [RegulatoryAgency].[SEC].issuers i on i.accessionnumber = d.accessionnumber
  where minimuminvestmentaccepted >= 100000
GO
