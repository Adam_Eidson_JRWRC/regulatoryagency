SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

       CREATE view [dbo].[vIndividualLicense_Flattened] as
		SELECT DISTINCT ST2.CRDNumber, 
				SUBSTRING(
				(
					SELECT ';'+REPLACE(ST1.LicenseName ,'Series ','')  AS [text()]
					FROM dbo.vindividualLicense ST1
					WHERE ST1.CRDNumber = ST2.CRDNumber
					ORDER BY ST1.CRDNumber
					FOR XML PATH ('')
				), 2, 1000) Licenses

            
        FROM dbo.vIndividualLicense ST2
GO
