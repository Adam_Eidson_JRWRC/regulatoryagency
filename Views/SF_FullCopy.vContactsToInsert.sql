SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [SF_FullCopy].[vContactsToInsert] as

	
	--capture Individuals who are active at these qualifying firms.
	select distinct 
	i.CRDNumber, 
	dbo.ProperCase(FirstName) as FirstName,
	dbo.ProperCase(MiddleName) as MiddleName, 
	dbo.ProperCase(LastName) as LastName,
	dbo.ProperCase(Suffix) as Suffix,
	FINRALink,
	i.FINRAStatus,
	i.SECStatus,
	dateBeganInIndustry,
	ilf.Licenses,
	RegulatoryAgency.dbo.ContactRecordType(i.FINRAStatus,i.SECStatus) as RecordType,
	vipf.PriorFirmName,
	f.FINRANumber
	From RegulatoryAgency.dbo.individual_Firm iff
	join RegulatoryAgency.dbo.individual i on i.individualId = iff.individualId
	join RegulatoryAgency.dbo.Firm f on f.firmId = iff.firmId
	left join RegulatoryAgency.dbo.vindividualLicense_Flattened ilf on ilf.CRDNumber = i.CRDNumber
	--get all Contact CRD's from SalesForce
	left join [SalesForce_Reporting].[SF_FULLCOPY].[vContactsWithCRD] cwc on cwc.CRD__c = i.CRDNumber
	left join RegulatoryAgency.dbo.vIndividualPriorFirm vipf on vipf.individualId = i.IndividualId
	where cast(f.FINRANumber as varchar(15)) in (--query SalesForce for all qualified firms that are identified for Contact insert
							select Firm_CRD__c
							From [SalesForce_Reporting].[SF_FULLCOPY].[vFirmsToSyncWithFINRASEC]
							)
							and iff.endDate is null
							and cwc.CRD__c is null
							and (i.FINRAStatus = 'Active'
								or i.SECStatus = 'Active')

GO
