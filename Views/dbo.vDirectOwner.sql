SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vDirectOwner] as
SELECT do.id as directOwnerId
      ,F.[FirmId],
	  f.name as FirmName,
f.FINRANumber as FirmCRD,
f.FINRAStatus as FirmFINRAStatus,
do.IndividualCRD,
do.Name as IndividualName,
do.Role as IndividualRole,
I.SECStatus as IndividualSECStatus,
i.finrastatus as IndividualFINRAStatus
  FROM [RegulatoryAgency].[FINRA].[DirectOwner] do
  join RegulatoryAgency.dbo.firm f on f.firmId = do.firmId
  left join RegulatoryAgency.dbo.Individual i on i.IndividualId = do.individualId


GO
