SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[vFirmInsert_CompetetiveSponsor] as 
with cte_Firms as (
select distinct f.FINRANumber as FirmCRDNumber,
f.Name as FirmName,
'https://brokercheck.finra.org/firm/summary/'+cast(f.FINRANumber as varchar(15)) as FirmFINRAURL,
FINRAStatus,
SECStatus,
HighNetWorthAuM,
NumberOfHighNetWorthClients
from RegulatoryAgency.dbo.firm f
  left join RegulatoryAgency.dbo.CompetetiveSponsor cs on cs.FirmCRDNumber = f.FINRANumber
where cs.CompetetiveSponsorID is null
)

select distinct cf.*,
STRING_AGG( cast(vfw.website as varchar(max)), ', ') as websites
from cte_Firms cf
left join (select distinct finranumber, website from FINRA.dbo.vFirmWebsite) vfw on vfw.FINRANumber = cf.FirmCRDNumber
group by
FirmName,
FirmFINRAURL,
FirmCRDNumber,
FINRAStatus,
SECStatus,
HighNetWorthAuM,
NumberOfHighNetWorthClients

GO
