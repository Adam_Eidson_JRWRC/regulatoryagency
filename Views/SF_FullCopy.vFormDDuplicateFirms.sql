SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FullCopy].[vFormDDuplicateFirms] as
with cte_FirmData as (

select distinct Firm_CRD__c, count(*) as count 
From openquery(SALESFORCEDEVARTFULLCOPY,'select id, firm_CRD__c  from Broker_Dealer__c where Firm_CRD__c != null')
group by Firm_CRD__c
having Count(*) >1
), cte_FirmDupes as (
select 'https://na57.salesforce.com/'+sf.id as SF_URL,
sf.id as FirmId,
[count],
sf.name,
sf.Firm_CRD__c
From openquery(SALESFORCEDEVARTFULLCOPY,'select id, firm_CRD__c, name  from Broker_Dealer__c where Firm_CRD__c != null') sf
join cte_FirmData fd on fd.firm_CRD__c = sf.Firm_CRD__c
)

select distinct Broker_Dealer__c, sf.Firm_CRD__c--crd__c--, count(*)
--'https://na57.salesforce.com/'+id
--into Contact
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, CRD__c, Broker_Dealer__c from Contact where CRD__c != null') s
join SF_FullCopy.vFirmsWPastBusinessOrSignedSA sf on sf.id = s.Broker_Dealer__c
left join cte_FirmDupes cfd on cfd.FirmId = sf.Id
where cfd.FirmId is not null
GO
