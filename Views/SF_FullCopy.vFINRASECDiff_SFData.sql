SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE View [SF_FullCopy].[vFINRASECDiff_SFData] as 
with cte_SFData as (
select cast(id as varchar(200)) as id,
CRD__c,
CAST(ISNULL(Licenses_Held__c, '') as varchar(200)) as Licenses_Held__c,
CAST(ISNULL(RecordTypeId, '') as varchar(200)) as RecordTypeId,
CAST(ISNULL(Dually_Registered__c, '') as varchar(200)) as Dually_Registered__c,
CAST(ISNULL(FINRA_Status__c, '') as varchar(200)) as FINRA_Status__c,
CAST(ISNULL(REPLACE(REPLACE(Phone,')',''),'(',''), '') as varchar(200)) as Phone,
CAST(ISNULL(Mailing_County__c, '') as varchar(200)) as Mailing_County__c,
CAST(ISNULL(MailingStreet, '') as varchar(200)) as MailingStreet,
CAST(ISNULL(MailingCity, '') as varchar(200)) as MailingCity ,
CAST(ISNULL(MailingState, '') as varchar(200)) as MailingState,
CAST(ISNULL(MailingPostalCode, '') as varchar(200)) as MailingPostalCode,
CAST(ISNULL(FINRA_Broker_Dealer_Branch_Street__c, '') as varchar(200)) as FINRA_Broker_Dealer_Branch_Street__c,
CAST(ISNULL(FINRA_Broker_Dealer_Branch_Street_2__c, '') as varchar(200)) as FINRA_Broker_Dealer_Branch_Street_2__c,
CAST(ISNULL(FINRA_Broker_Dealer_Branch_City__c, '') as varchar(200)) as FINRA_Broker_Dealer_Branch_City__c,
CAST(ISNULL(FINRA_Broker_Dealer_Branch_State__c, '') as varchar(200)) as FINRA_Broker_Dealer_Branch_State__c,
CAST(ISNULL(FINRA_Broker_Dealer_Branch_Postal_Code__c, '') as varchar(200)) as FINRA_Broker_Dealer_Branch_Postal_Code__c,
CAST(ISNULL(FINRA_RIA_Branch_Street__c, '') as varchar(200)) as FINRA_RIA_Branch_Street__c,
CAST(ISNULL(FINRA_RIA_Branch_Street_2__c, '') as varchar(200)) as FINRA_RIA_Branch_Street_2__c,
CAST(ISNULL(FINRA_RIA_Branch_City__c, '') as varchar(200)) as FINRA_RIA_Branch_City__c,
CAST(ISNULL(FINRA_RIA_Branch_State__c, '') as varchar(200)) as FINRA_RIA_Branch_State__c,
CAST(ISNULL(FINRA_RIA_Branch_Postal_Code__c, '') as varchar(200)) as FINRA_RIA_Branch_Postal_Code__c,
CAST(ISNULL(BD_Firm__c, '') as varchar(200)) as BD_Firm__c,
CAST(ISNULL(Broker_Dealer__c, '') as varchar(200)) as Broker_Dealer__c,
CAST(ISNULL(RIA_Firm__c, '') as varchar(200)) as RIA_Firm__c,
CAST(ISNULL(Prior_Firm_Name__c, '') as varchar(200)) as Prior_Firm_Name__c,
CAST(ISNULL(Date_Became_Rep__c, '') as varchar(200)) as Date_Became_Rep__c,
CAST(ISNULL(BD_Date_of_Hire__c, '') as varchar(200)) as BD_Date_of_Hire__c,
CAST(ISNULL(FINRA_Detailed_Report_URL__c, '') as varchar(200)) as FINRA_Detailed_Report_URL__c--,
--cast(ISNULL(Total_Form_D_Sales_Commissions__c,'') as varchar(200)) as Total_Form_D_Sales_Commissions__c
From openQuery(SALESFORCEDEVARTFULLCOPY,'select id, CRD__c, 
Licenses_Held__c, 
RecordTypeId, 
Dually_Registered__c, 
FINRA_Status__c,
Phone,
Mailing_County__c,
MailingStreet,
MailingCity,
MailingState,
MailingPostalCode,
FINRA_Broker_Dealer_Branch_Street__c,
FINRA_Broker_Dealer_Branch_Street_2__c,
FINRA_Broker_Dealer_Branch_City__c,
FINRA_Broker_Dealer_Branch_State__c,
FINRA_Broker_Dealer_Branch_Postal_Code__c,
FINRA_RIA_Branch_Street__c,
FINRA_RIA_Branch_Street_2__c,
FINRA_RIA_Branch_City__c,
FINRA_RIA_Branch_State__c,
FINRA_RIA_Branch_Postal_Code__c,
BD_Firm__c,
Broker_Dealer__c,
RIA_Firm__c,
Prior_Firm_Name__c,
Date_Became_Rep__c,
BD_Date_of_Hire__c,
FINRA_Detailed_Report_URL__c,
Assign_Sales_Territory__c
from Contact where CRD__c is not null')
), cte_FirmData as (

select * 
From openquery(SALESFORCEDEVARTFULLCOPY,'select id, firm_CRD__c  from Broker_Dealer__c where Firm_CRD__c is not null')
), cte_FirmAssociation as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY,'select id, Broker_Dealer__c, Contact__c from BD_RIA_Association__c')
), cte_BDRecordType as (
select *, 'Broker-Dealer' as RecordTypeName 
From openquery(SALESFORCEDEVARTFULLCOPY,'select RecordTypeId from Contact where RecordTypeId in (SELECT Id FROM RecordType WHERE Name =  ''Broker-Dealer'') group by RecordTypeId')
), cte_RIARecordType as (
select *, 'RIA' as RecordTypeName 
From openquery(SALESFORCEDEVARTFULLCOPY,'select RecordTypeId from Contact where RecordTypeId in (SELECT Id FROM RecordType WHERE Name = ''RIA'') group by RecordTypeId')
), cte_DuallyRegisteredRecordType as (
select *, 'Dually Registered' as RecordTypeName 
From openquery(SALESFORCEDEVARTFULLCOPY,'select RecordTypeId from Contact where RecordTypeId in (SELECT Id FROM RecordType WHERE Name = ''Dually Registered'') group by RecordTypeId')
), cte_SFDataFinal as (
select csd.*,
cast(cfd.firm_CRD__c as varchar(200)) as firm_CRD__c,
case when cbdr.RecordTypeName is not null
	then cast(cbdr.RecordTypeName as varchar(200))
	when crr.RecordTypeName is not null
	then cast(crr.RecordTypeName as varchar(200))
	when cdrr.RecordTypeName is not null
	then cast(cdrr.RecordTypeName as varchar(200))
	end as RecordType
From cte_SFData csd
--left join cte_FirmAssociation cfa on cfa.Contact__c = csd.id
left join cte_FirmData cfd on cfd.id = csd.Broker_Dealer__c
left join cte_BDRecordType cbdr on cbdr.RecordTypeId = csd.RecordTypeId
left join cte_RIARecordType crr on crr.RecordTypeId = csd.RecordTypeId
left join cte_DuallyRegisteredRecordType cdrr on cdrr.RecordTypeId = csd.RecordTypeId
)


--------SELECT name
--------FROM   tempdb.sys.columns
--------WHERE  object_id = Object_id('tempdb..#temp')
--------and name not in ('CRD__c')
Select CRD__c,
'http://cs17.salesforce.com/'+id as SFURL,
id as SFID,
ColumnName,
ColumnValue
FROM (select *
From cte_SFDataFinal) p
UNPIVOT
(ColumnValue FOR ColumnName in 
	( 
BD_Date_of_Hire__c,
firm_CRD__c,
Broker_Dealer__c,
Date_Became_Rep__c,
Dually_Registered__c,
FINRA_Broker_Dealer_Branch_City__c,
FINRA_Broker_Dealer_Branch_Postal_Code__c,
FINRA_Broker_Dealer_Branch_State__c,
FINRA_Broker_Dealer_Branch_Street__c,
FINRA_Broker_Dealer_Branch_Street_2__c,
FINRA_Detailed_Report_URL__c,
FINRA_RIA_Branch_City__c,
FINRA_RIA_Branch_Postal_Code__c,
FINRA_RIA_Branch_State__c,
FINRA_RIA_Branch_Street__c,
FINRA_RIA_Branch_Street_2__c,
FINRA_Status__c,

Licenses_Held__c,
Mailing_County__c,
MailingCity,
MailingPostalCode,
MailingState,
MailingStreet,
Phone,
Prior_Firm_Name__c,
RecordType,
RIA_Firm__c--,
--Total_Form_D_Sales_Commissions__c
)
	) as unpvt
GO
