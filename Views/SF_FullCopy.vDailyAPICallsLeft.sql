SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FullCopy].[vDailyAPICallsLeft] as 
select 70000-count(distinct CRD) as APICallsLeft
From RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking
where lastQuery >  DATEADD(day, -1,GETDATE())
GO
