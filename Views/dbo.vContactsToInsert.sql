SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [dbo].[vContactsToInsert] as

with cte_KeepCRD as (
SELECT distinct [CRDNumber]      
  FROM [RegulatoryAgency].[dbo].[vIndividualLicense]
  where REPLACE(LicenseName, 'Series ','') in ('7','22','7TO','22TO','65')
  ), cte_ValidLicense as (
  select vilf.*
  from [RegulatoryAgency].[dbo].[vIndividualLicense_Flattened] vilf
  left join cte_KeepCRD ckc on ckc.CRDNumber = vilf.CRDNumber
  where ckc.CRDNumber is not null
  )


	
	--capture Individuals who are active at these qualifying firms.
	select prep.CRDNumber,
	prep.FirstName,
	MiddleName,
	LastName,
	Suffix,
	FINRALink as FINRA_Detailed_Report_URL__c,
	Status__c, 
	dateBeganInIndustry as Date_Became_Rep__c,
	Licenses as Licenses_Held__c,
	PriorFirmName as [Prior_Firm_Name__c],
	crt.id as RecordTypeId 
	from (select distinct 
	i.CRDNumber, 
	dbo.ProperCase(i.FirstName) as FirstName,
	dbo.ProperCase(MiddleName) as MiddleName, 
	dbo.ProperCase(i.LastName) as LastName,
	dbo.ProperCase(Suffix) as Suffix,
	FINRALink,
case when cvl.Licenses is not null
then 'active'
else 'inactive'
end as Status__c,
	dateBeganInIndustry,
	ilf.Licenses,
	RegulatoryAgency.dbo.ContactRecordType(i.FINRAStatus,i.SECStatus) as RecordType,
	vipf.PriorFirmName,
	f.FINRANumber
	From RegulatoryAgency.dbo.individual_Firm iff
	join RegulatoryAgency.dbo.individual i on i.individualId = iff.individualId
	join RegulatoryAgency.dbo.Firm f on f.firmId = iff.firmId
	left join RegulatoryAgency.dbo.vindividualLicense_Flattened ilf on ilf.CRDNumber = i.CRDNumber
	--get all Contact CRD's from SalesForce
	left join [SalesForce_Reporting].dbo.[vContactsWithCRD] cwc on cwc.CRD__c = i.CRDNumber
	left join RegulatoryAgency.dbo.vIndividualPriorFirm vipf on vipf.individualId = i.IndividualId
	left join cte_ValidLicense cvl on cvl.CRDNumber = i.CRDNumber
	where cast(f.FINRANumber as varchar(15)) in (--query SalesForce for all qualified firms that are identified for Contact insert
							select Firm_CRD__c
							From [SalesForce_Reporting].dbo.[vFirmsToSyncWithFINRASEC]
							)
							and iff.endDate is null
							and cwc.CRD__c is null
							and (i.FINRAStatus = 'Active'
								or i.SECStatus = 'Active')
								) prep
								
	left join SalesForce_Reporting.dbo.vContactRecordType crt on crt.name = prep.RecordType

GO
