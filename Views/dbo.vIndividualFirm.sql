SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vIndividualFirm] as 
select i.FirstName, 
i.LastName,
i.CRDNumber as individualCRDNumber,
i.FINRAStatus as IndividualFinraStatus,
i.SECStatus as IndividualSECStatus,
f.name as firmName,
f.FINRANumber as firmCRDNumber,
f.FINRAStatus as FirmFINRAStatus,
f.SECStatus as FirmSECStatus,
ifirm.StartDate,
ifirm.EndDate
From Individual i
join Individual_Firm ifirm on ifirm.IndividualId = i.IndividualId
join firm f on f.FirmId = ifirm.FirmId
GO
