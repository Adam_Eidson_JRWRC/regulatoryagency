SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [SEC].[vQualifiedFormD_IndividualData] as
with cte_FormDFirmAtTimeOfSalescommission as (
SELECT count( r.ACCESSIONNUMBER) as TotalFormDSalesCommissions,  
vqr.*,
i.dateBeganInIndustry,
DATEDIFF(year,i.dateBeganInIndustry,GETDATE()) as yearsInIndustry,
case when ifirm.EndDate is null
	Then DATEDIFF(year, ifirm.StartDate, GETDATE())
	when ifirm.EndDate is not null
	THEN DATEDIFF(year, ifirm.StartDate, ifirm.EndDate)
	END
	as YearsAtFirmOfSalesCommission,
i.FINRAStatus as IndividualFINRAStatus,
i.SECStatus as IndividualSECStatus,
f.Name as firmAtTimeOfSalesCommission,
f.FINRANumber as firmAtTimeOfSalesCommissionFINRANumber,
f.FINRAStatus as FirmAtTimeOfSalesCommissionFINRAStatus,
f.SECStatus as FirmAtTimeOfSalesCommissionSECStatus,
ifirm.startDate as FirmAtTimeOfSalesCommission_StartDate,
ifirm.EndDate as FirmAtTimeOfSalesCommission_EndDate
  FROM [RegulatoryAgency].[SEC].[vQualifiedFormD_REIT] vqr
  join RegulatoryAgency.dbo.individual i on i.CRDNumber = vqr.crdnumber
  join RegulatoryAgency.SEC.RECIPIENTS r on (
											convert(varchar(50),r.ASSOCIATEDBDCRDNUMBER) = convert(varchar(50),i.CRDNumber) 
											or convert(varchar(50),r.RECIPIENTCRDNUMBER) = convert(varchar(50),i.crdnumber)
											)
  join RegulatoryAgency.SEC.FORMDSUBMISSION fds on fds.ACCESSIONNUMBER = r.ACCESSIONNUMBER
  left join RegulatoryAgency.dbo.Individual_Firm Ifirm on ifirm.IndividualId = i.IndividualId and fds.FILING_DATE between ifirm.StartDate and ISNULL(ifirm.EndDate, GETDATE())
  left join RegulatoryAgency.dbo.firm f on f.FirmId = ifirm.FirmId
  --where i.CRDNumber = 5389090
  group by vqr.crdnumber,
  f.name,
  ifirm.startDate,
ifirm.EndDate,
i.dateBeganInIndustry,
i.FINRAStatus,
i.SECStatus,
f.FINRANumber,
f.FINRAStatus,
f.SECStatus											
), cte_RankedIndividualFirm as (
select individualCRDNumber,
ROW_NUMBER() over(partition by individualCRDNumber order by isnull(endDate,GETDATE()) desc) as employmentRow,
firstName,
LastName,
firmName as CurrentFirmName,
FirmCRDNumber as CurrentFirmCRD,
firmFINRAStatus as CurrentFirmFINRAStatus,
FirmSECStatus as CurrentFirmSECStatus,
StartDate as CurrentFirmStartDate,
EndDate as CurrentFirmEndDate
From RegulatoryAgency.dbo.vIndividualFirm
), cte_MostRecentIndividualFirm as (
select *
From cte_RankedIndividualFirm
where employmentRow = 1
)
select c.*,
vif.firstName,
vif.LastName,
'https://brokercheck.finra.org/individual/summary/'+cast(c.crdnumber as varchar(15)) as IndividualFINRAURL,
vif.currentFirmName,
vif.currentFirmFINRAStatus,
vif.currentFirmSECStatus,
vif.currentFirmStartDate,
vif.currentFirmEndDate
From cte_FormDFirmAtTimeOfSalescommission c
left join cte_mostRecentIndividualFirm vif on vif.individualCRDNumber = c.CRDNumber
left join RegulatoryAgency.dbo.CompetetiveSponsor cs on cs.FirmCRDNumber = vif.CurrentFirmCRD
where cs.FirmCRDNumber is null
GO
