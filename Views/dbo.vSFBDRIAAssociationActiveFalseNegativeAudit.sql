SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Script for SelectTopNRows command from SSMS  ******/
CREATE View [dbo].[vSFBDRIAAssociationActiveFalseNegativeAudit] as 

  with cte_SF_BDRIA as (
  select distinct vcd.CRD__c,
vbd.Firm_CRD__c, active__c,
vcd.id as contact__c,
vbd.id as broker_Dealer__c,
bdria.id as BDRIAAssociationId
From SalesForce_Reporting.dbo.vContactsWithCRD vcd
join SalesForce_Reporting.dbo.vBD_RIA_Association bdria on vcd.id = bdria.Contact__c
join SalesForce_Reporting.dbo.vBDRIAAssociationRecordType bdriart on bdria.recordtypeid = bdriart.id
join SalesForce_Reporting.dbo.vBrokerDealer vbd on  vbd.id = bdria.Broker_Dealer__c
where vcd.CRD__c is not null
and vbd.firm_CRD__c is not null

and bdriart.name = 'FINRA/SEC'
), cte_RegAgency_BDRIA as (
SELECT vif.[FirstName]
      ,vif.[LastName]
      ,[individualCRDNumber]
      ,[IndividualFinraStatus]
      ,[IndividualSECStatus]
      ,[firmName]
      ,[firmCRDNumber]
      ,[FirmFINRAStatus]
      ,[FirmSECStatus]
      ,[StartDate]
      ,[EndDate]
  FROM [RegulatoryAgency].[dbo].[vIndividualFirm] vif
  join SalesForce_Reporting.dbo.vContactsWithCRD vcd on vcd.CRD__c = vif.individualCRDNumber --only grab RA records that are already in SF
  where (FirmFINRAStatus = 'Active'
  or FirmSECStatus = 'Active')
  and (IndividualFinraStatus = 'Active'
  or  IndividualSECStatus = 'Active')
  and endDate is null
  )

  select *
  From cte_RegAgency_BDRIA ra
  left join cte_SF_BDRIA sf on ra.individualCRDNumber = sf.CRD__c
								and sf.Firm_CRD__c = ra.firmCRDNumber
								where crd__c is null
								or active__c = 0
GO
