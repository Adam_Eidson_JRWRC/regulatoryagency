SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vFirmsToDelete] as
with cte_Firms as (
select *
From openquery(SALESFORCEDEVART,'select id, firm_CRD__c, CreatedDate, Key_Account__c from Broker_Dealer__c where CreatedById = ''0050b000005imtW''')
), cte_individuals as (
select *
From openquery(SALESFORCEDEVART,'select id, Broker_Dealer__c, phone from Contact')

)

select distinct f.id
From cte_Firms f
left join cte_individuals i on i.Broker_Dealer__c = f.id
--join RegulatoryAgency.dbo.firm fi on fi.FINRANumber = f.firm_CRD__c
where i.id is null
and Key_Account__c = 0
and convert(date, createdDate) between '2019-10-22' and '2019-10-24'
GO
