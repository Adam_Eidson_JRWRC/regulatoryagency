SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

create VIEW [dbo].[vQualifiedOfferingDetails] as 
with cte_RecipientCRD as (
select distinct  I.ENTITYNAME,
I.ENTITYTYPE,
Fds.FILE_NUM,
fds.FILING_DATE,
O.[ACCESSIONNUMBER]
      ,[INDUSTRYGROUPTYPE]
      ,[INVESTMENTFUNDTYPE]
      ,[IS40ACT]
      ,[REVENUERANGE]
      ,[AGGREGATENETASSETVALUERANGE]
      ,[FEDERALEXEMPTIONS_ITEMS_LIST]
      ,[ISAMENDMENT]
      ,[PREVIOUSACCESSIONNUMBER]
      ,[SALE_DATE]
      ,[YETTOOCCUR]
      ,[MORETHANONEYEAR]
      ,[ISEQUITYTYPE]
      ,[ISDEBTTYPE]
      ,[ISOPTIONTOACQUIRETYPE]
      ,[ISSECURITYTOBEACQUIREDTYPE]
      ,[ISPOOLEDINVESTMENTFUNDTYPE]
      ,[ISTENANTINCOMMONTYPE]
      ,[ISMINERALPROPERTYTYPE]
      ,[ISOTHERTYPE]
      ,[DESCRIPTIONOFOTHERTYPE]
      ,[ISBUSINESSCOMBINATIONTRANS]
      ,[BUSCOMBCLARIFICATIONOFRESP]
      ,[MINIMUMINVESTMENTACCEPTED]
      ,[OVER100RECIPIENTFLAG]
      ,[TOTALOFFERINGAMOUNT]
      ,[TOTALAMOUNTSOLD]
      ,[TOTALREMAINING]
      ,[SALESAMTCLARIFICATIONOFRESP]
      ,[HASNONACCREDITEDINVESTORS]
      ,[NUMBERNONACCREDITEDINVESTORS]
      ,[TOTALNUMBERALREADYINVESTED]
      ,[SALESCOMM_DOLLARAMOUNT]
      ,[SALESCOMM_ISESTIMATE]
      ,[FINDERSFEE_DOLLARAMOUNT]
      ,[FINDERSFEE_ISESTIMATE]
      ,[FINDERFEECLARIFICATIONOFRESP]
      ,[GROSSPROCEEDSUSED_DOLLARAMOUNT]
      ,[GROSSPROCEEDSUSED_ISESTIMATE]
      ,[GROSSPROCEEDSUSED_CLAROFRESP]
      ,REPLACE(REPLACE([AUTHORIZEDREPRESENTATIVE],CHAR(10),''),CHAR(13),'') as [AUTHORIZEDREPRESENTATIVE]
FROM RegulatoryAgency.SEC.FORMDSUBMISSION fds
  left join RegulatoryAgency.SEC.ISSUERS I on I.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  left join RegulatoryAgency.SEC.OFFERING O on O.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  left join RegulatoryAgency.SEC.RECIPIENTS R on R.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  where O.ISEQUITYTYPE = 'True'
  and O.INDUSTRYGROUPTYPE = 'REITs and Finance'
  and O.FEDERALEXEMPTIONS_ITEMS_LIST like '%6b%'
  and RECIPIENTCRDNUMBER is not null
  and RECIPIENTCRDNUMBER not in ('', 'None', 'N\A', 'N/A')
  and ENTITYTYPE != 'Corporation'
  --and O.MINIMUMINVESTMENTACCEPTED >= 50000
  --order by FILING_DATE desc
  ),  cte_AssociatedBDCRDNumber as (
SELECT distinct  I.ENTITYNAME,
I.ENTITYTYPE,
Fds.FILE_NUM,
fds.FILING_DATE,
	O.[ACCESSIONNUMBER]
      ,[INDUSTRYGROUPTYPE]
      ,[INVESTMENTFUNDTYPE]
      ,[IS40ACT]
      ,[REVENUERANGE]
      ,[AGGREGATENETASSETVALUERANGE]
      ,[FEDERALEXEMPTIONS_ITEMS_LIST]
      ,[ISAMENDMENT]
      ,[PREVIOUSACCESSIONNUMBER]
      ,[SALE_DATE]
      ,[YETTOOCCUR]
      ,[MORETHANONEYEAR]
      ,[ISEQUITYTYPE]
      ,[ISDEBTTYPE]
      ,[ISOPTIONTOACQUIRETYPE]
      ,[ISSECURITYTOBEACQUIREDTYPE]
      ,[ISPOOLEDINVESTMENTFUNDTYPE]
      ,[ISTENANTINCOMMONTYPE]
      ,[ISMINERALPROPERTYTYPE]
      ,[ISOTHERTYPE]
      ,[DESCRIPTIONOFOTHERTYPE]
      ,[ISBUSINESSCOMBINATIONTRANS]
      ,[BUSCOMBCLARIFICATIONOFRESP]
      ,[MINIMUMINVESTMENTACCEPTED]
      ,[OVER100RECIPIENTFLAG]
      ,[TOTALOFFERINGAMOUNT]
      ,[TOTALAMOUNTSOLD]
      ,[TOTALREMAINING]
      ,[SALESAMTCLARIFICATIONOFRESP]
      ,[HASNONACCREDITEDINVESTORS]
      ,[NUMBERNONACCREDITEDINVESTORS]
      ,[TOTALNUMBERALREADYINVESTED]
      ,[SALESCOMM_DOLLARAMOUNT]
      ,[SALESCOMM_ISESTIMATE]
      ,[FINDERSFEE_DOLLARAMOUNT]
      ,[FINDERSFEE_ISESTIMATE]
      ,[FINDERFEECLARIFICATIONOFRESP]
      ,[GROSSPROCEEDSUSED_DOLLARAMOUNT]
      ,[GROSSPROCEEDSUSED_ISESTIMATE]
      ,[GROSSPROCEEDSUSED_CLAROFRESP]
      ,REPLACE(REPLACE([AUTHORIZEDREPRESENTATIVE],CHAR(10),''),CHAR(13),'') as [AUTHORIZEDREPRESENTATIVE]
  FROM RegulatoryAgency.SEC.FORMDSUBMISSION fds
  left join RegulatoryAgency.SEC.ISSUERS I on I.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  left join RegulatoryAgency.SEC.OFFERING O on O.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  left join RegulatoryAgency.SEC.RECIPIENTS R on R.ACCESSIONNUMBER = fds.ACCESSIONNUMBER
  where O.ISEQUITYTYPE = 'True'
  and O.INDUSTRYGROUPTYPE = 'REITs and Finance'
  and O.FEDERALEXEMPTIONS_ITEMS_LIST like '%6b%'
  --and O.MINIMUMINVESTMENTACCEPTED >= 50000
  and ASSOCIATEDBDCRDNUMBER is not null
  and ASSOCIATEDBDCRDNUMBER not in ('', 'None', 'N\A', 'N/A')
  and ENTITYTYPE != 'Corporation'
  )

  Select *
  From (select *
  From cte_RecipientCRD
  union 
  select *
  From cte_AssociatedBDCRDNumber
  ) s

GO
