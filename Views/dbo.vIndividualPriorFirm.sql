SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [dbo].[vIndividualPriorFirm] as
with cte_previousFirm as (
select max(endDate) as MostREcentEndDate,
IndividualId
From RegulatoryAgency.dbo.individual_Firm iff
	where iff.enddate is not null
	group by individualId
)

select distinct i.IndividualId,
f.name as PriorFirmName
From cte_previousFirm cpf
join RegulatoryAgency.dbo.individual_Firm iff on iff.IndividualId = cpf.IndividualId
												and cpf.MostREcentEndDate = iff.EndDate
	join RegulatoryAgency.dbo.individual i on i.individualId = iff.individualId
	join RegulatoryAgency.dbo.Firm f on f.firmId = iff.firmId
GO
