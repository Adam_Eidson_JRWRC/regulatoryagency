SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE View [dbo].[vFINRASECDiff_WarehouseData_NSC] as 
with cte_License as (
SELECT CRDNumber,
STRING_AGG(License,';')as Licenses
from (
Select distinct CRDNumber,
REPLACE(Name, 'Series ','') as License
from RegulatoryAgency.dbo.Individual_License il 
join RegulatoryAgency.dbo.License l on l.LicenseId = il.LicenseId
join RegulatoryAgency.dbo.Individual i on i.IndividualId = il.IndividualId
) as l
group by CRDNumber

), cte_PriorFirm_prep as (
select ROW_NUMBER() over(partition by i.crdNumber order by EndDate desc) as rowNum,
i.CRDNumber,
f.Name
from RegulatoryAgency.dbo.Individual_firm iff
join RegulatoryAgency.dbo.Individual i on i.IndividualId = iff.IndividualId
join RegulatoryAgency.dbo.Firm f on f.FirmId = iff.FirmId
--join cte_SFData csd on csd.CRD__c = i.CRDNumber
where iff.EndDate is not null 

), cte_PriorFirm as (
select CRDNumber, Name as PriorFirmName
from cte_PriorFirm_prep cpfp
where rowNum = 1
), cte_WarehouseData as (
select distinct  i.CRDNumber,
cast(case when i.SECStatus = 'Active'
or i.FINRAStatus = 'Active'
Then 'Active'
else 'inactive'
end as varchar(200)) as FINRA_Status__c,
cast(case when i.SECStatus = 'Active'
and i.FINRAStatus = 'Active'
Then 'Dually Registered'
when i.SECStatus = 'Active'
and i.FINRAStatus != 'Active'
Then 'RIA'
when i.SECStatus != 'Active'
and i.FINRAStatus = 'Active'
Then 'Broker-Dealer'
else 'Unknown' 
end as varchar(200)) as RecordType,
cast(i.SECStatus as varchar(200)) as SECStatus,
cast(i.FINRAStatus as varchar(200)) as FINRAStatus,
cast(i.dateBeganInIndustry as varchar(200)) as Date_Became_Rep__c,
cast(MAX(iff.StartDate) as Varchar(200)) as BD_Date_of_Hire__c,
cast(f.FINRANumber as varchar(200)) as Firm_CRD__c,
case when ibl.street2 is not null
then cast(ibl.Street1+', '+ibl.Street2  as varchar(200)) 
else  cast(ibl.Street1 as varchar(200))
end as MailingStreet,
cast(ibl.Street2 as varchar(200)) as ibl_Street2,
cast(ibl.City as varchar(200)) as MailingCity,
cast(ibl.postalCode as varchar(200)) as MailingPostalCode,
cast(ibl.State as varchar(200)) as MailingState,
cast(REPLACE(REPLACE(fa.PhoneNumber,'(',''),')','') as varchar(200)) as Phone,
cast(fa.Street1 as varchar(200)) as Street1,
cast(fa.Street2 as varchar(200)) as Street2,
cast(fa.City as varchar(200)) as FirmAddressCity,
cast(fa.State as varchar(200)) as FirmAddressState,
cast(fa.PostalCode as varchar(200)) as FirmAddressPostalCode,
cast(fa.Country as varchar(200)) as Country,
cast(fa.county as varchar(200)) as county,
cast(fa.MainAddress as varchar(200)) as MainAddress,
cast(fa.isIAAddress as varchar(200)) as isIAAddress,
--cast(cl.Licenses as varchar(200)) as Licenses_Held__c,
--cast(do.Role as varchar(200)) as Role,
cast(cpf.PriorFirmName as varchar(200)) as Prior_Firm_Name__c,
cast('https://brokercheck.finra.org/individual/summary/'+cast(i.CRDNumber as varchar(15)) as varchar(200)) as FINRA_Detailed_Report_URL__c,
cast(SUM(vqfdid.TotalFormDSalescommissions) as varchar(200)) as totalFormDSalesCommissions
From RegulatoryAgency.dbo.individual i
--join cte_License cl on cl.CRDNumber = i.CRDNumber
join RegulatoryAgency.dbo.Individual_Firm iff on iff.IndividualId = i.IndividualId
												and iff.EndDate is null
join RegulatoryAgency.dbo.firm f on f.FirmId = iff.FirmId 
left join RegulatoryAgency.dbo.Individual_BranchLocation ibl on ibl.FirmId = f.FirmId
and ibl.IndividualId = i.IndividualId
and LocatedAtFlag = 1
left join RegulatoryAgency.dbo.Firm_Address fa on fa.FirmId = f.FirmId
												and fa.isIAAddress = CASE WHEN i.SECStatus = 'Active'
																				and i.FINRAStatus != 'Active'
																		Then 1
																		WHEN i.FINRAStatus = 'Active'
																		Then 0
																		End
--left join RegulatoryAgency.FINRA.DirectOwner do on do.IndividualCRD = i.CRDNumber
--													and do.FirmId = f.firmId
left join cte_PriorFirm cpf on cpf.CRDNumber = i.IndividualId
left join [RegulatoryAgency].[SEC].[vQualifiedFormD_IndividualData] vqfdid on vqfdid.CRDNumber = i.CRDNumber
--join cte_SFData t on t.CRD__c = i.crdNumber

where f.FINRANumber =  7569
--do.role is not null
group by i.CRDNumber,
i.SECStatus,
i.FINRAStatus,
i.dateBeganInIndustry,
f.FINRANumber,
ibl.Street1,
ibl.Street2,
ibl.City,
ibl.postalCode,
ibl.State,
fa.PhoneNumber,
fa.Street1,
fa.Street2,
fa.City,
fa.State,
fa.PostalCode,
fa.Country,
fa.county,
fa.MainAddress,
fa.isIAAddress,
--cl.Licenses,
--do.Role,
cpf.PriorFirmName
)
--SELECT name
--FROM   tempdb.sys.columns
--WHERE  object_id = Object_id('tempdb..#temp_dw')
--and name not in ('CRDNumber')

Select CRDNumber,
ColumnName,
ColumnValue
FROM (select *
From cte_WarehouseData) p
UNPIVOT
(ColumnValue FOR ColumnName in 
	(
Country,
county,
Date_Became_Rep__c,
BD_Date_of_Hire__c,
FINRA_Status__c,
RecordType,
FINRAStatus,
Firm_CRD__c,
FirmAddressCity,
FirmAddresspostalCode,
FirmAddressState,
Street1,
Street2,
isIAAddress,
--Licenses_Held__c,
MailingCity,
MailingPostalCode,
MailingState,
MainAddress,
Phone,
--Role,
SECStatus,
MailingStreet,
Prior_Firm_Name__c,
FINRA_Detailed_Report_URL__c,
TotalFormDSalesCommissions)
	) as unpvt
GO
