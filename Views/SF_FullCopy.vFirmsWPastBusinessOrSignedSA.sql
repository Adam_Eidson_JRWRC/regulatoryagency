SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FullCopy].[vFirmsWPastBusinessOrSignedSA] as
with cte_FirmsWSellingAgreement as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c, All_Signed_Selling_Agreements__c from Broker_Dealer__c where Firm_CRD__c != null and All_Signed_Selling_Agreements__c != ''None''')
),cte_Firms as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c, All_Signed_Selling_Agreements__c from Broker_Dealer__c where Firm_CRD__c != null ')
), cte_Investors as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Broker_Dealer_ER__c from Account where Broker_Dealer_ER__c != null and stage__c = ''Closed and Funded''')
)

select cast(id as varchar(50)) as id,
cast(Firm_CRD__c as varchar(50)) as Firm_CRD__c
From cte_FirmsWSellingAgreement
union
select cast(cf.id as varchar(50)) as id,
cast(cf.Firm_CRD__c as varchar(50)) as Firm_CRD__c
from cte_Firms cf
join cte_Investors ci on ci.Broker_Dealer_ER__c = cf.id
GO
