SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [dbo].[vIndividualLicense] as

select distinct 
i.CRDNumber,
l.name as LicenseName
from individual_License il
join individual i on i.IndividualId = il.IndividualId
join license l on l.LicenseId = il.LicenseId
GO
