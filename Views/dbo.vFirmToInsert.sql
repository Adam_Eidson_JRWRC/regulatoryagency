SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vFirmToInsert] as
SELECT distinct vif.firmCRDNumber
From dbo.vIndividualFirm vif --records that we know about in the local datastore. Firms and Contacts
join SalesForce_Reporting.dbo.vContactsWithCRD sfContact on sfContact.CRD__c = vif.individualCRDNumber --contacts that exist in SF.
left join SalesForce_Reporting.dbo.vDistinctFirmCRD vdfc on convert(varchar(15),vdfc.Firm_CRD__c) = convert(varchar(15),vif.FirmCRDNumber )--firms that exist in SF
where vdfc.Firm_CRD__c is null --does not exist in SF
and vif.EndDate is null --still active at firm
GO
