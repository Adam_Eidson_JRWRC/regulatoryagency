SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [SF_FullCopy].[vLicenseToUpdate] as
/*
-----------------------------
-----------------------------
License Data
-----------------------------
-----------------------------
*/

SELECT distinct vilf.Licenses as Licenses_Held__c,
SFContact.id
from cdc.dbo_Individual_License_CT cil
join RegulatoryAgency.dbo.individual i on i.individualid = cil.IndividualId
join RegulatoryAgency.dbo.vIndividualLicense_Flattened vilf on vilf.CRDNumber = i.CRDNumber
join SalesForce_Reporting.SF_FULLCOPY.vContactsWithCRD SFContact on SFContact.CRD__c = i.CRDNumber
where sys.fn_cdc_map_lsn_to_time([__$start_lsn]) >= DATEADD(day,-5,GETDATE())

GO
