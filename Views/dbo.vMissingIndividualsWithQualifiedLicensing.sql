SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vMissingIndividualsWithQualifiedLicensing] as
with cte_KeepCRD as (
SELECT distinct [CRDNumber]      
  FROM [RegulatoryAgency].[dbo].[vIndividualLicense]
  where REPLACE(LicenseName, 'Series ','') in ('7','22','7TO','22TO','65')
  ), cte_ValidLicense as (
  select vilf.*
  from [RegulatoryAgency].[dbo].[vIndividualLicense_Flattened] vilf
  left join cte_KeepCRD ckc on ckc.CRDNumber = vilf.CRDNumber
  where ckc.CRDNumber is not null
  )

  select distinct cvl.*
  From RegulatoryAgency.dbo.vIndividualFirm vif
  join cte_ValidLicense cvl on cvl.CRDNumber = vif.individualCRDNumber
  join SalesForce_Reporting.dbo.vDistinctFirmCRD vdf on vdf.Firm_CRD__c = vif.firmCRDNumber
  left join SalesForce_Reporting.dbo.vContactsWithCRD sfContact on sfContact.CRD__c = cvl.CRDNumber
  where sfContact.CRD__c is null
  and vif.EndDate is null
GO
