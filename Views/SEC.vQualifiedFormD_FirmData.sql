SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [SEC].[vQualifiedFormD_FirmData] as 
with cte_Firms as (
SELECT distinct count(r.ACCESSIONNUMBER) as totalFormDSalescommissions,
AVG(O.MINIMUMINVESTMENTACCEPTED) as AverageMinimumInvestmentAccepted,
f.Name as FirmName,
'https://brokercheck.finra.org/firm/summary/'+cast(f.FINRANumber as varchar(15)) as FirmFINRAURL,
f.FINRANumber as FirmCRDNumber,
f.SECNumber as FirmSECNumber,
f.FINRAStatus,
f.SECStatus,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
fa.PhoneNumber
  FROM [RegulatoryAgency].[SEC].[vQualifiedFormD_REIT] vqr
    join RegulatoryAgency.dbo.Firm f on f.FINRANumber = vqr.crdnumber
  join RegulatoryAgency.SEC.RECIPIENTS r on (
											convert(varchar(50),r.ASSOCIATEDBDCRDNUMBER) = convert(varchar(50),f.FINRANumber) 
											or convert(varchar(50),r.RECIPIENTCRDNUMBER) = convert(varchar(50),f.FINRANumber)
											)
	join RegulatoryAgency.SEC.OFFERING o on o.ACCESSIONNUMBER = r.ACCESSIONNUMBER
  join RegulatoryAgency.SEC.FORMDSUBMISSION fds on fds.ACCESSIONNUMBER = r.ACCESSIONNUMBER
  left join RegulatoryAgency.dbo.Firm_Address fa on fa.FirmId = f.FirmId and fa.PhoneNumber is not null
  left join RegulatoryAgency.dbo.CompetetiveSponsor cs on cs.FirmCRDNumber = f.FINRANumber
  where cs.FirmCRDNumber is null
  
  group by f.Name,
f.FINRANumber ,
f.SECNumber ,
f.FINRAStatus,
f.SECStatus,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
fa.PhoneNumber
)

select cf.*,
STRING_AGG( cast(vfw.website as varchar(max)), ', ') as websites
from cte_Firms cf
left join (select distinct finranumber, website from FINRA.dbo.vFirmWebsite) vfw on vfw.FINRANumber = cf.FirmCRDNumber
group by  totalFormDSalescommissions,
AverageMinimumInvestmentAccepted,
FirmName,
FirmFINRAURL,
FirmCRDNumber,
FirmSECNumber,
FINRAStatus,
SECStatus,
HighNetWorthAuM,
NumberOfHighNetWorthClients,
phonenumber
--order by totalFormDSalescommissions desc
GO
