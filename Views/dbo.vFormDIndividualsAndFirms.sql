SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

create view [dbo].[vFormDIndividualsAndFirms] as
SELECT r.*, minimuminvestmentaccepted
  FROM [RegulatoryAgency].[dbo].[RECIPIENTS] r
  join FormDSubmission d on d.AccessionNumber  = r.accessionNumber
  join offering o on o.accessionnumber = d.accessionnumber
  join issuers i on i.accessionnumber = d.accessionnumber
  where minimuminvestmentaccepted >= 100000
GO
