SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDailyAPICallsLeft] as 
select 70000-count(distinct CRD) as APICallsLeft
From RegulatoryAgency.dbo.CRDRefreshTracking
where lastQuery >  DATEADD(day, -1,GETDATE())
GO
