SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE View [SF_FullCopy].[vFirmInsert_SFPrepped] as 
with cte_Firms as (
select distinct 
--Get OwnerId
--'FINRA/SEC ETL' as LeadSource,--Get LeadSource
RegulatoryAgency.SF_FullCopy.FirmRecordType_SFID(f.FINRAStatus, f.SECStatus) as RecordTypeId,
RegulatoryAgency.SF_FullCopy.FirmFINRAStatus(f.FINRAStatus, f.SECStatus)  as FINRA_Status__c,
--Get RecordTypeId
f.FINRANumber as Firm_CRD__c,
f.Name as Name,
--'https://brokercheck.finra.org/firm/summary/'+cast(f.FINRANumber as varchar(15)) as Firm_FINRA_URL__c,
--f.FINRAStatus as FINRA_Status__c,
--f.SECStatus as SEC_Status__c, --To be leveraged in the future
case when f.SECStatus = 'Active'
	then 'https://adviserinfo.sec.gov/firm/summary/'+cast(f.FINRANumber as varchar(15))
	else null
	end as Firm_SEC_URL__c,
cast(f.HighNetWorthAuM as varchar(15)) as Firm_High_Net_Worth_AUM__c,
f.NumberOfHighNetWorthClients as Firm_of_High_Net_Worth_Clients__c,
ffd.totalFormDSalescommissions as Total_Form_D_Sales_Commissions__c,
replace(replace(replace(replace(f.SECNumber, '801-',''),'802-',''), CHAR(13), ''), CHAR(10),'') as Firm_SEC__c
from RegulatoryAgency.dbo.firm f
join RegulatoryAgency.dbo.vIndividualFirm vif on vif.firmCRDNumber = f.finraNumber--records that we know about in the local datastore. Firms and Contacts
join SalesForce_Reporting.SF_FULLCOPY.vContactsWithCRD sfContact on sfContact.CRD__c = vif.individualCRDNumber --contacts that exist in SF.
left join SalesForce_Reporting.SF_FULLCOPY.vDistinctFirmCRD vdfc on convert(varchar(15),vdfc.Firm_CRD__c) = convert(varchar(15),vif.FirmCRDNumber )--firms that exist in SF
left join RegulatoryAgency.[SEC].[vFormD_FirmData] ffd on ffd.FirmCRDNumber = vif.firmCRDNumber
where vdfc.Firm_CRD__c is null
and vif.EndDate is null --still active at firm
and (f.FINRAStatus is not null
or f.SECStatus is not null)

)

select distinct cf.*,
STRING_AGG( cast(vfw.website as varchar(max)), ', ') as website__c
from cte_Firms cf
left join (select distinct finranumber, website from FINRA.dbo.vFirmWebsite) vfw on vfw.FINRANumber = cf.Firm_CRD__c
group by
--LeadSource,
Name,
--Firm_FINRA_URL__c,
Firm_SEC_URL__c,
Firm_CRD__c,
RecordTypeId,
FINRA_Status__c,
--SEC_Status__c, --To be leveraged in the future
Firm_High_Net_Worth_AUM__c,
Firm_of_High_Net_Worth_Clients__c,
Total_Form_D_Sales_Commissions__c,
Firm_SEC__c

GO
